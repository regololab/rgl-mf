<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>REGOLO Mini-Framework</title>
        <meta name="AUTHOR" content="" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link rel="icon" href="<?php echo $Rgl->this_page()['relative'] ?>img/rgl_mf.svg" type="image/svg" />
        <!--  EXTERNAL CSS (bootstrap, Foundation, Skeleton...) -->
        <link rel="stylesheet" type="text/css" href="<?php echo $Rgl->this_page()['relative'] ?>css/css.php" >
    </head>
<body style="padding:10px;">
    <h1 style="background:#002255;padding:5px;text-align:center;">
        <img src="<?php echo $Rgl->this_page()['relative'] ?>img/rgl_mf.svg" alt="logo Regolo Framework PHP - MTV" style="width:25%;">
    </h1><br>
    <h3>
        <?php echo $_['benvenuto'] ?> <br><?php echo $_['generate'] ?>:
        <a href="<?php echo _CONFIG_['_ADMIN_'] ?>index.php" target="_blank"><b>Admin</b></a>
    </h3><br>
    <code>
        - Username: admin<br>
        - Password: admin<br> <br>
    </code>


    <?php echo Avvisi::print_message(); ?>
    <script type="text/javascript" src="<?php echo $Rgl->this_page()['relative'] ?>js/rgl.js"></script>
</body>
</html>
