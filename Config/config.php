<?php
// GESTIONE ERRORI
ini_set('display_errors', 'On');
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRECATED);
// Content-Type
header('Content-Type: text/html; charset=utf-8');
// GESTIONE CACHE
//ottenere l'ultima modifica aggiornata di questo file
$lastModified=filemtime(__FILE__);
//ottenere un hash unico di questo file
$etagFile = md5_file(__FILE__);
//ottenere l'intestazione HTTP_IF_MODIFIED_SINCE se impostato
$ifModifiedSince=(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? htmlspecialchars($_SERVER['HTTP_IF_MODIFIED_SINCE']) : false);
//ottenere l'intestazione HTTP_IF_NONE_MATCH se impostata (ETAG: hash di file unico)
$etagHeader=(isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim(htmlspecialchars($_SERVER['HTTP_IF_NONE_MATCH'])) : false);
//Setta l'ultima modifica dell HEADER
header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");
//Setta etag-header
header("Etag: $etagFile");
//assicurarsi che la cache sia attiva
header('Cache-Control: public');
//verificare se la pagina � cambiata. In caso contrario, inviare 304 e esce
if(@strtotime(htmlspecialchars($_SERVER['HTTP_IF_MODIFIED_SINCE']))==$lastModified || $etagHeader == $etagFile){
    header("HTTP/1.1 304 Not Modified");
    exit;
}

// indirizzi pagine principali
define('ROOT_PATH', explode('Config',__DIR__)[0]);
define('ADDRESS_SITE', 'http://localhost/rgl-mf/'); # modify http or https
define ("_CONFIG_",
    array(
        // gestione sessioni
        "_ELW_" => "exists_login_{{ELW}}",
        "_KYE_" => "key_{{KYE}}_exists",
        "ENCRYPTION_METHOD" => "AES-256-CBC",
        "ENCRYPTION_KEY" => "{{KEY}}",#your secret key
        "ENCRYPTION_IV" => "{{IV}}",
        "_SESSION_NAME_" => 'Rglab_0133'.str_replace('.', '', htmlspecialchars($_SERVER['REMOTE_ADDR'])),
        // lingua
        "_DEFAULT_LANG_" => "it",
        "_TIME_ZONE_" => 'Europe/Rome', 
        // versione e sito
        "_PRODUCT_" => 'REGOLO mf',
        "_VERSION_" => 'beta 0.4.3',
        "_NOME_SITO_" => 'NOME SITO',
        // path & breadcrubs calculate
        // in root './'; in sub dir -> '/name_project/'; in sub/sub dir -> '/name_project/dir/' etc.
        '_HOME_' => '/rgl-mf/',
        // path
        '_ROOT_' => str_replace("//", "/", ROOT_PATH.'/'),
        '_CONF_' => str_replace("//", "/", ROOT_PATH.'/'.'Config/'),
        '_IMAGES_' => str_replace("//", "/", ROOT_PATH.'/'.'img/'),
        '_LANG_' => str_replace("//", "/", ROOT_PATH.'/'.'Config/lang//'),
        // address
        '_SITE_' => ADDRESS_SITE,
        '_LOGOUT_' => ADDRESS_SITE.'',
        '_ADMIN_' => ADDRESS_SITE.'Admin-rgl/',
        '_ERROR_404_' => ADDRESS_SITE.'Error404.php',
        // LOGIN
        '_TABLE_' => '{{LOGINS}}',
        '_USER_INPUT_' => 'username', // name input user
        '_PASS_INPUT_' => 'password', // name input password
        // Db connect
        '_DB_' => array(
            '_PREFIX_' => '{{PREFIX}}',
            # DB -> PDO ---------
            '_TIPO_DB_' => 'SQLITE', # MYSQL, SQLITE, PGSQL
            # if SQLITE insert path db eg.: str_replace("//", "/", ROOT_PATH.'/').'db/' else host address
            '_HOST_DB_' => str_replace("//", "/", ROOT_PATH.'/').'db/',#str_replace("//", "/", ROOT_PATH.'/').'db/',
            '_USER_DB_' => '',
            '_PASS_DB_' => '',
            '_NAME_DB_' => 'base.sqlite',
            '_PORT_DB_' => '5432',
            # DB -> MYSQL - MARIADB ---------
            '_TIPO_MY_' => 'MYSQL',
            '_HOST_MY_' => '',
            '_USER_MY_' => '',
            '_PASS_MY_' => '',
            '_NAME_MY_' => '',
            # DB -> SQLITE ---------
            '_TIPO_LTE_' => 'SQLITE',
            '_HOST_LTE_' => str_replace("//", "/", ROOT_PATH.'/').'db/',
            '_NAME_LTE_' => '',
            # DB -> POSTGRESQL ---------
            '_TIPO_PG_' => 'PGSQL',
            '_HOST_PG_' => '',
            '_USER_PG_' => '',
            '_PASS_PG_' => '',
            '_NAME_PG_' => '',
            '_PORT_PG_' => '5432',
            # General DB
            '_ERR_QUERY_' => true,
        ),
        // Private page Generate.php
        '_USERN_' => 'admin',
        '_PASSW_' => 'admin',
        // email address
        '_ADMIN_MAIL_' => '',
        '_CONTACT_MAIL_' => '',
        // paging default
        '_PAGINAZIONE_10_' => '10',
        // MAX dim image uopz_overload
        '_FILE_SIZE_' => '10485760',
        '_IMAGE_W_' => '750',
        '_IMAGE_H_' => '518',
    )
);

ini_set('session.use_cookies', 1);
ini_set('session.use_only_cookies', 1);
#ini_set('session.cache_limiter', 'private'); // non utilizzare mai per aree private!!!
session_name(_CONFIG_['_SESSION_NAME_']);
session_start();
function set_language(){if(!$_SESSION['Lang']) $_SESSION['Lang'] = _CONFIG_['_DEFAULT_LANG_'];}
set_language();

/*
Regolo mini framework
LICENSE MIT:
Copyright 2017 Regololab.com
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

Except as contained in this notice, the name(s) of the above copyright holders shall not be used in advertising or otherwise to promote the sale,
use or other dealings in this Software without prior written authorization.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
?>
