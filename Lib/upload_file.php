<?php
// 2 mb = 270000;

class UploadFile extends Avvisi {

    public $tipo;
    public $peso;
    public $width;
    public $height;
    public $mode;
    public $translate;

    public function __construct($arr = false) {
        $this->tipo=$arr['t'];
        $this->peso=$arr['s'];
        $this->width=$arr['w'];
        $this->height=$arr['h'];
        $this->mode=$arr['m'];
    }

    public function get_file($file,$nome,$directoty,$pre_name=false) {
        $estenzione=UploadFile::file_extension($file[$nome]['name']);
        $peso=$file[$nome]['size'];
        // se esiste il pre_name lo inserisco nel nome altrimenti formo solo un nome casuale
        $prename=($pre_name) ? ($pre_name) : ("");
        $nome_file=$prename.date('dmYhis')."_".rand(11111,99999);

        if($file[$nome]['tmp_name'] && $directoty) {
            // controllo estensione
            if(in_array($estenzione,$this->tipo)) {
                // controllo se il valore peso è stato specificato
                if($this->peso > 0) {
                    // verifico se il peso del file sia uguale o minore al valore da noi specificato
                    if($this->peso >= $peso) {
                        // se è una immagine e non ho impostato le poporzioni la carico così come è
                        if(@move_uploaded_file($file[$nome]['tmp_name'], $directoty.$nome_file.".".$estenzione)) {
                            @chmod($directoty.$nome_file.".".$estenzione,0777); //permessi per poterla inserire
                        }
                        // restituisco il nome del file
                        return $nome_file.".".$estenzione;
                    } else {
                        Avvisi::message(Language::translate()['SuperaPeso']." ".$this->peso." byte!", 'warning.php');
                    }
                } else {
                    // se non ho impostato il peso del file, la carico così come è
                    if(@move_uploaded_file($file[$nome]['tmp_name'], $directoty.$nome_file.".".$estenzione)) {
                        @chmod($directoty.$nome_file.".".$estenzione,0777); //permessi per poterla inserire
                    }
                    // restituisco il nome del file
                    return $nome_file.".".$estenzione;
                }
            } else {
                Avvisi::message(Language::translate()['FileExtDiverso'], 'warning.php');
            }
        }
    }

    public function send_file($file,$nome,$directoty,$pre_name=false) {
        include_once(_CONFIG_['_LANG_'].$_SESSION['Lang'].'.php');
        $this->translate=$this->translate;
        $estenzione=UploadFile::file_extension($file[$nome]['name']);
        $size = @getimagesize($file[$nome]['tmp_name']);
        $realW = $size[0];
        $realH = $size[1];

        $peso=$file[$nome]['size'];
        // se esiste il pre_name lo inserisco nel nome altrimenti formo solo un nome casuale
        $prename=($pre_name) ? ($pre_name) : ("");
        $nome_img=$prename.date('dmYhis')."_".rand(11111,99999);

        if($file[$nome]['tmp_name'] && $directoty) {
            // controllo estensione
            if(in_array($estenzione,$this->tipo)) {
                // controllo se il valore peso è stato specificato
                if($this->peso > 0) {
                    // verifico se il peso del file sia uguale o minore al valore da noi specificato
                    if($this->peso >= $peso) {
                        // se di tipo immagine provo a ridimensionarlo nelle dimensioni richieste
                        if(preg_match("@image@i",$file[$nome]['type'])) {
                            // verifico quali dimensioni vengono specificate nella gestione immagine
                                // se esistono entrambe allora ridimensiono entrambi i lati
                                if($this->width > 0 && $this->height > 0 && $this->mode) {

                                    // carico l'immagine nella cartella
                                    if(@move_uploaded_file($file[$nome]['tmp_name'], $directoty.$nome_img.".".$estenzione)) {
                                        @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                                    }

                                    // qui ridimensiono l'immagine in base alle dimensioni assegnate nella classe
                                    switch ($estenzione) {
                                        case "gif": $src = @imagecreatefromgif($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "JPG ": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpeg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case"png": $src = @imagecreatefrompng($directoty.$nome_img.".".$estenzione);
                                        break;
                                    }
                                    $out=@imagecreatetruecolor($this->width,$this->height);
                                    @imagealphablending($out, false);
                                    @imagesavealpha($out, true);
                                    @imagecopyresampled($out,$src,0,0,0,0,$this->width,$this->height,imagesx($src),imagesy($src));

                                    switch ($estenzione) {
                                        case "gif": @imagegif($out, $directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                        break;
                                        case "JPG ": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                        break;
                                        case "jpeg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                        break;
                                        case"png": @imagepng($out, $directoty.$nome_img.".".$estenzione);
                                        break;
                                    }

                                    // restituisco il nome dell'immagine
                                    return $nome_img.".".$estenzione;

                                } else if($this->width && !$this->height && $this->mode){
                                // se esiste l'altezza la modifico ed in proporzione anche la larghezza
                                // carico l'immagine nella cartella

                                    if(@move_uploaded_file($file[$nome]['tmp_name'], $directoty.$nome_img.".".$estenzione)) {
                                        @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                                    }

                                    // qui ridimensiono l'immagine in base alle dimensioni assegnate nella classe
                                    #echo $directoty.$nome_img.".".$estenzione;
                                    switch ($estenzione) {
                                        case "gif": $src = @imagecreatefromgif($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "JPG ": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpeg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case"png": $src = @imagecreatefrompng($directoty.$nome_img.".".$estenzione);
                                        break;
                                    }

                                    $larghezza_originale=@imagesx($src);
                                    $altezza_originale=@imagesy($src);

                                    // verifico quale lato è il più alto
                                    if($larghezza_originale >= $altezza_originale) {
                                        $proporzioni_altezza=($altezza_originale*$this->width)/$larghezza_originale;
                                        $out=@imagecreatetruecolor($this->width,$proporzioni_altezza);
                                        @imagealphablending($out, false);
                                        @imagesavealpha($out, true);
                                        @imagecopyresampled($out,$src,0,0,0,0,$this->width,$proporzioni_altezza,imagesx($src),imagesy($src));

                                        switch ($estenzione) {
                                            case "gif": @imagegif($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                            case "jpg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "JPG ": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "jpeg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case"png": @imagepng($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                        }
                                        // restituisco il nome dell'immagine
                                        return $nome_img.".".$estenzione;
                                    } else {
                                        $proporzioni_larghezza=($larghezza_originale*$this->width)/$altezza_originale;
                                        $out=@imagecreatetruecolor($proporzioni_larghezza,$this->width);
                                        @imagecopyresampled($out,$src,0,0,0,0,$proporzioni_larghezza,$this->width,imagesx($src),imagesy($src));

                                        switch ($estenzione) {
                                            case "gif": @imagegif($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                            case "jpg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "JPG ": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "jpeg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case"png": @imagepng($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                        }
                                        // restituisco il nome dell'immagine
                                        return $nome_img.".".$estenzione;
                                    }
                                } elseif($this->width > 0 && $this->height > 0 && !$this->mode) {
                                    // verifico che l'immagine non sia piu grande delle dimensioni reali
                                    if($this->width > $realW) $this->width = $realW;
                                    if($this->height > $realH) $this->height = $realH;

                                    // carico l'immagine nella cartella
                                    if(@move_uploaded_file($file[$nome]['tmp_name'], $directoty.$nome_img.".".$estenzione)) {
                                        @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                                    }

                                    // qui ridimensiono l'immagine in base alle dimensioni assegnate nella classe
                                    switch ($estenzione) {
                                        case "gif": $src = @imagecreatefromgif($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "JPG ": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpeg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case"png": $src = @imagecreatefrompng($directoty.$nome_img.".".$estenzione);
                                        break;
                                    }
                                    $out=@imagecreatetruecolor($this->width,$this->height);
                                    @imagealphablending($out, false);
                                    @imagesavealpha($out, true);
                                    @imagecopyresampled($out,$src,0,0,0,0,$this->width,$this->height,imagesx($src),imagesy($src));

                                    switch ($estenzione) {
                                        case "gif": @imagegif($out, $directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                        break;
                                        case "JPG ": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                        break;
                                        case "jpeg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                        break;
                                        case"png": @imagepng($out, $directoty.$nome_img.".".$estenzione);
                                        break;
                                    }

                                    // restituisco il nome dell'immagine
                                    return $nome_img.".".$estenzione;


                                } else if($this->width && !$this->height && !$this->mode){
                                    // se esiste l'altezza la modifico ed in proporzione anche la larghezza

                                    // verifico che l'immagine non sia piu grande delle dimensioni reali
                                    if($this->width > $realW) $this->width = $realW;
                                    if($this->height > $realH) $this->height = $realH;

                                    // carico l'immagine nella cartella

                                    if(@move_uploaded_file($file[$nome]['tmp_name'], $directoty.$nome_img.".".$estenzione)) {
                                        @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                                    }

                                    // qui ridimensiono l'immagine in base alle dimensioni assegnate nella classe
                                    switch ($estenzione) {
                                        case "gif": $src = @imagecreatefromgif($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "JPG ": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpeg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case"png": $src = @imagecreatefrompng($directoty.$nome_img.".".$estenzione);
                                        break;
                                    }
                                    $larghezza_originale=@imagesx($src);
                                    $altezza_originale=@imagesy($src);

                                    // verifico quale lato è il più alto
                                    if($larghezza_originale >= $altezza_originale) {
                                        $proporzioni_altezza=($altezza_originale*$this->width)/$larghezza_originale;
                                        $out=@imagecreatetruecolor($this->width,$proporzioni_altezza);
                                        @imagealphablending($out, false);
                                        @imagesavealpha($out, true);
                                        @imagecopyresampled($out,$src,0,0,0,0,$this->width,$proporzioni_altezza,imagesx($src),imagesy($src));

                                        switch ($estenzione) {
                                            case "gif": @imagegif($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                            case "jpg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "JPG ": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "jpeg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case"png": @imagepng($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                        }
                                        // restituisco il nome dell'immagine
                                        return $nome_img.".".$estenzione;
                                    } else {
                                        $proporzioni_larghezza=($larghezza_originale*$this->width)/$altezza_originale;
                                        $out=@imagecreatetruecolor($proporzioni_larghezza,$this->width);
                                        @imagecopyresampled($out,$src,0,0,0,0,$proporzioni_larghezza,$this->width,imagesx($src),imagesy($src));

                                        switch ($estenzione) {
                                            case "gif": @imagegif($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                            case "jpg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "JPG ": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "jpeg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case"png": @imagepng($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                        }
                                        // restituisco il nome dell'immagine
                                        return $nome_img.".".$estenzione;
                                    }
                                } else {
                                    //se è una immagine e non ho impostato le poporzioni la carico così come è
                                    if(@move_uploaded_file($file[$nome]['tmp_name'], $directoty.$nome_img.".".$estenzione)) {
                                        @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                                    }
                                    // restituisco il nome dell'immagine
                                    return $nome_img.".".$estenzione;
                                }
                        } else {
                            // se è una immagine e non ho impostato le poporzioni la carico così come è
                            if(@move_uploaded_file($file[$nome]['tmp_name'], $directoty.$nome_img.".".$estenzione)) {
                                @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                            }
                            // restituisco il nome dell'immagine
                            return $nome_img.".".$estenzione;
                        }
                    } else {
                        Avvisi::message(Language::translate()['SuperaPeso']." ".$this->peso." byte!", 'warning.php');
                    }
                } else {
                    // se non ho impostato il peso dell'immagine, la carico così come è
                    if(@move_uploaded_file($file[$nome]['tmp_name'], $directoty.$nome_img.".".$estenzione)) {
                        @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                    }
                    // restituisco il nome dell'immagine
                    return $nome_img.".".$estenzione;
                }
            } else {
                Avvisi::message(Language::translate()['FileExtDiverso'], 'warning.php');
            }
        }
    }

    public function send_file_multiple($file,$n,$nome,$directoty,$pre_name=false) {
        include_once(_CONFIG_['_LANG_'].$_SESSION['Lang'].'.php');
        $this->translate=$this->translate;
        $estenzione=UploadFile::file_extension($file[$nome]['name'][$n]);
        $size = @getimagesize($file[$nome]['tmp_name'][$n]);
        $realW = $size[0];
        $realH = $size[1];

        $peso=$file[$nome]['size'][$n];
        // se esiste il pre_name lo inserisco nel nome altrimenti formo solo un nome casuale
        $prename=($pre_name) ? ($pre_name) : ("");
        $nome_img=$prename.date('dmYhis')."_".rand(11111,99999);

        if($file[$nome]['tmp_name'][$n] && $directoty) {
            // controllo estensione
            if(in_array($estenzione,$this->tipo)) {
                // controllo se il valore peso è stato specificato
                if($this->peso > 0) {
                    // verifico se il peso del file sia uguale o minore al valore da noi specificato
                    if($this->peso >= $peso) {
                        // se di tipo immagine provo a ridimensionarlo nelle dimensioni richieste
                        if(preg_match("@image@i",$file[$nome]['type'][$n])) {
                            // verifico quali dimensioni vengono specificate nella gestione immagine
                                // se esistono entrambe allora ridimensiono entrambi i lati
                                if($this->width > 0 && $this->height > 0 && $this->mode) {

                                    // carico l'immagine nella cartella
                                    if(@move_uploaded_file($file[$nome]['tmp_name'][$n], $directoty.$nome_img.".".$estenzione)) {
                                        @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                                    }

                                    // qui ridimensiono l'immagine in base alle dimensioni assegnate nella classe
                                    switch ($estenzione) {
                                        case "gif": $src = @imagecreatefromgif($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "JPG ": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpeg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case"png": $src = @imagecreatefrompng($directoty.$nome_img.".".$estenzione);
                                        break;
                                    }
                                    $out=@imagecreatetruecolor($this->width,$this->height);
                                    @imagealphablending($out, false);
                                    @imagesavealpha($out, true);
                                    @imagecopyresampled($out,$src,0,0,0,0,$this->width,$this->height,imagesx($src),imagesy($src));

                                    switch ($estenzione) {
                                        case "gif": @imagegif($out, $directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                        break;
                                        case "JPG ": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                        break;
                                        case "jpeg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                        break;
                                        case"png": @imagepng($out, $directoty.$nome_img.".".$estenzione);
                                        break;
                                    }

                                    // restituisco il nome dell'immagine
                                    return $nome_img.".".$estenzione;

                                } else if($this->width && !$this->height && $this->mode){
                                // se esiste l'altezza la modifico ed in proporzione anche la larghezza
                                // carico l'immagine nella cartella

                                    if(@move_uploaded_file($file[$nome]['tmp_name'][$n], $directoty.$nome_img.".".$estenzione)) {
                                        @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                                    }

                                    // qui ridimensiono l'immagine in base alle dimensioni assegnate nella classe
                                    #echo $directoty.$nome_img.".".$estenzione;
                                    switch ($estenzione) {
                                        case "gif": $src = @imagecreatefromgif($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "JPG ": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpeg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case"png": $src = @imagecreatefrompng($directoty.$nome_img.".".$estenzione);
                                        break;
                                    }

                                    $larghezza_originale=@imagesx($src);
                                    $altezza_originale=@imagesy($src);

                                    // verifico quale lato è il più alto
                                    if($larghezza_originale >= $altezza_originale) {
                                        $proporzioni_altezza=($altezza_originale*$this->width)/$larghezza_originale;
                                        $out=@imagecreatetruecolor($this->width,$proporzioni_altezza);
                                        @imagealphablending($out, false);
                                        @imagesavealpha($out, true);
                                        @imagecopyresampled($out,$src,0,0,0,0,$this->width,$proporzioni_altezza,imagesx($src),imagesy($src));

                                        switch ($estenzione) {
                                            case "gif": @imagegif($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                            case "jpg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "JPG ": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "jpeg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case"png": @imagepng($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                        }
                                        // restituisco il nome dell'immagine
                                        return $nome_img.".".$estenzione;
                                    } else {
                                        $proporzioni_larghezza=($larghezza_originale*$this->width)/$altezza_originale;
                                        $out=@imagecreatetruecolor($proporzioni_larghezza,$this->width);
                                        @imagecopyresampled($out,$src,0,0,0,0,$proporzioni_larghezza,$this->width,imagesx($src),imagesy($src));

                                        switch ($estenzione) {
                                            case "gif": @imagegif($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                            case "jpg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "JPG ": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "jpeg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case"png": @imagepng($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                        }
                                        // restituisco il nome dell'immagine
                                        return $nome_img.".".$estenzione;
                                    }
                                } elseif($this->width > 0 && $this->height > 0 && !$this->mode) {
                                    // verifico che l'immagine non sia piu grande delle dimensioni reali
                                    if($this->width > $realW) $this->width = $realW;
                                    if($this->height > $realH) $this->height = $realH;

                                    // carico l'immagine nella cartella
                                    if(@move_uploaded_file($file[$nome]['tmp_name'][$n], $directoty.$nome_img.".".$estenzione)) {
                                        @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                                    }

                                    // qui ridimensiono l'immagine in base alle dimensioni assegnate nella classe
                                    switch ($estenzione) {
                                        case "gif": $src = @imagecreatefromgif($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "JPG ": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpeg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case"png": $src = @imagecreatefrompng($directoty.$nome_img.".".$estenzione);
                                        break;
                                    }
                                    $out=@imagecreatetruecolor($this->width,$this->height);
                                    @imagealphablending($out, false);
                                    @imagesavealpha($out, true);
                                    @imagecopyresampled($out,$src,0,0,0,0,$this->width,$this->height,imagesx($src),imagesy($src));

                                    switch ($estenzione) {
                                        case "gif": @imagegif($out, $directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                        break;
                                        case "JPG ": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                        break;
                                        case "jpeg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                        break;
                                        case"png": @imagepng($out, $directoty.$nome_img.".".$estenzione);
                                        break;
                                    }

                                    // restituisco il nome dell'immagine
                                    return $nome_img.".".$estenzione;


                                } else if($this->width && !$this->height && !$this->mode){
                                    // se esiste l'altezza la modifico ed in proporzione anche la larghezza

                                    // verifico che l'immagine non sia piu grande delle dimensioni reali
                                    if($this->width > $realW) $this->width = $realW;
                                    if($this->height > $realH) $this->height = $realH;

                                    // carico l'immagine nella cartella

                                    if(@move_uploaded_file($file[$nome]['tmp_name'][$n], $directoty.$nome_img.".".$estenzione)) {
                                        @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                                    }

                                    // qui ridimensiono l'immagine in base alle dimensioni assegnate nella classe
                                    switch ($estenzione) {
                                        case "gif": $src = @imagecreatefromgif($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "JPG ": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case "jpeg": $src = @imagecreatefromjpeg($directoty.$nome_img.".".$estenzione);
                                        break;
                                        case"png": $src = @imagecreatefrompng($directoty.$nome_img.".".$estenzione);
                                        break;
                                    }
                                    $larghezza_originale=@imagesx($src);
                                    $altezza_originale=@imagesy($src);

                                    // verifico quale lato è il più alto
                                    if($larghezza_originale >= $altezza_originale) {
                                        $proporzioni_altezza=($altezza_originale*$this->width)/$larghezza_originale;
                                        $out=@imagecreatetruecolor($this->width,$proporzioni_altezza);
                                        @imagealphablending($out, false);
                                        @imagesavealpha($out, true);
                                        @imagecopyresampled($out,$src,0,0,0,0,$this->width,$proporzioni_altezza,imagesx($src),imagesy($src));

                                        switch ($estenzione) {
                                            case "gif": @imagegif($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                            case "jpg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "JPG ": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "jpeg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case"png": @imagepng($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                        }
                                        // restituisco il nome dell'immagine
                                        return $nome_img.".".$estenzione;
                                    } else {
                                        $proporzioni_larghezza=($larghezza_originale*$this->width)/$altezza_originale;
                                        $out=@imagecreatetruecolor($proporzioni_larghezza,$this->width);
                                        @imagecopyresampled($out,$src,0,0,0,0,$proporzioni_larghezza,$this->width,imagesx($src),imagesy($src));

                                        switch ($estenzione) {
                                            case "gif": @imagegif($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                            case "jpg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "JPG ": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case "jpeg": @imagejpeg($out, $directoty.$nome_img.".".$estenzione, 85);
                                            break;
                                            case"png": @imagepng($out, $directoty.$nome_img.".".$estenzione);
                                            break;
                                        }
                                        // restituisco il nome dell'immagine
                                        return $nome_img.".".$estenzione;
                                    }
                                } else {
                                    //se è una immagine e non ho impostato le poporzioni la carico così come è
                                    if(@move_uploaded_file($file[$nome]['tmp_name'][$n], $directoty.$nome_img.".".$estenzione)) {
                                        @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                                    }
                                    // restituisco il nome dell'immagine
                                    return $nome_img.".".$estenzione;
                                }
                        } else {
                            // se è una immagine e non ho impostato le poporzioni la carico così come è
                            if(@move_uploaded_file($file[$nome]['tmp_name'][$n], $directoty.$nome_img.".".$estenzione)) {
                                @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                            }
                            // restituisco il nome dell'immagine
                            return $nome_img.".".$estenzione;
                        }
                    } else {
                        Avvisi::message(Language::translate()['SuperaPeso']." ".$this->peso." byte!", 'warning.php');
                    }
                } else {
                    // se non ho impostato il peso dell'immagine, la carico così come è
                    if(@move_uploaded_file($file[$nome]['tmp_name'][$n], $directoty.$nome_img.".".$estenzione)) {
                        @chmod($directoty.$nome_img.".".$estenzione,0777); //permessi per poterla inserire
                    }
                    // restituisco il nome dell'immagine
                    return $nome_img.".".$estenzione;
                }
            } else {
                Avvisi::message(Language::translate()['FileExtDiverso'], 'warning.php');
            }
        }
    }

    // questa funzione crea thumb delle immagini
    // copia l'immagine esistente ed aggiunge un suffisso a piacere
    public function resize_image($directory, $file, $pre_name, $width=false,$height=false) {
        // copio e rinomino il file
        @copy($directory.$file, $directory.$pre_name.$file);
        $files = $directory.$pre_name.$file;
        $extension = UploadFile::file_extension($files);

        // qui ridimensiono l'immagine se dichiaro larghezza e altezza
        if($width && $height) {

            // qui ridimensiono l'immagine in base alle dimensioni assegnate nella classe
            switch ($extension) {
                case "gif": $src = @imagecreatefromgif($files);
                break;
                case "jpg": $src = @imagecreatefromjpeg($files);
                break;
                case "JPG": $src = @imagecreatefromjpeg($files);
                break;
                case "jpeg": $src = @imagecreatefromjpeg($files);
                break;
                case"png": $src = @imagecreatefrompng($files);
                break;
            }

            $out = @imagecreatetruecolor($width,$height);
            imagealphablending($out, false);
            imagesavealpha($out, true);
            @imagecopyresampled($out,$src,0,0,0,0,$width,$height,imagesx($src),imagesy($src));

            switch ($extension) {
                case "gif": @imagegif($out, $files);
                break;
                case "jpg": @imagejpeg($out, $files, 85);
                break;
                case "JPG": @imagejpeg($out, $files, 85);
                break;
                case "jpeg": @imagejpeg($out, $files, 85);
                break;
                case"png": @imagepng($out, $files);
                break;
                }

        } else if ($width && !$height) { // qui ridimensiono l'immagine se dichiaro solo un elemento di altezza o larghezza
            // qui ridimensiono l'immagine in base alle dimensioni assegnate nella classe
            switch ($extension) {
                case "gif": $src = @imagecreatefromgif($files);
                break;
                case "jpg": $src = @imagecreatefromjpeg($files);
                break;
                case "JPG":  $src = @imagecreatefromjpeg($files);
                break;
                case "jpeg": $src = @imagecreatefromjpeg($files);
                break;
                case"png": $src = @imagecreatefrompng($files);
                break;
            }

            $larghezza_originale=@imagesx($src);
            $altezza_originale=@imagesy($src);

            // verifico quale lato è il più alto
            if($larghezza_originale >= $altezza_originale) {
                $proporzioni_altezza=($altezza_originale*$width)/$larghezza_originale;
                $out=@imagecreatetruecolor($width,$proporzioni_altezza);
                @imagealphablending($out, false);
                @imagesavealpha($out, true);
                @imagecopyresampled($out,$src,0,0,0,0,$width,$proporzioni_altezza,imagesx($src),imagesy($src));

                switch ($extension)
                {
                    case "gif": @imagegif($out, $files);
                    break;
                    case "jpg": @imagejpeg($out, $files, 85);
                    break;
                    case "JPG": @imagejpeg($out, $files, 85);
                    break;
                    case "jpeg": @imagejpeg($out, $files, 85);
                    break;
                    case"png": @imagepng($out, $files);
                    break;
                }
            } else {
                $proporzioni_larghezza=($larghezza_originale*$width)/$altezza_originale;
                $out=@imagecreatetruecolor($proporzioni_larghezza,$width);
                @imagealphablending($out, false);
                @imagesavealpha($out, true);
                @imagecopyresampled($out,$src,0,0,0,0,$proporzioni_larghezza,$width,imagesx($src),imagesy($src));

                switch ($extension) {
                    case "gif": @imagegif($out, $files);
                    break;
                    case "jpg": @imagejpeg($out, $files, 85);
                    break;
                    case "JPG": @imagejpeg($out, $files, 85);
                    break;
                    case "jpeg": @imagejpeg($out, $files, 85);
                    break;
                    case"png": @imagepng($out, $files);
                    break;
                }
            }
        }
    }

    //ImageCrop($arr, $dir, $name) $arr['w'], $arr['y']; /home/image/; 'immagine'
    public function crop_image($arr, $dir, $name) {
        $size = @getimagesize($dir.$name);
        $realW = $size[0];
        $realH = $size[1];
        $division = $realW / $realH;
        $image_asp = $arr['w'] / $arr['h'];
        if ( $division >= $image_asp ){
           $H = $arr['h'];
           $W = $realW / ($realH / $arr['h']);
        } else {
           $W = $arr['w'];
           $H = $realH / ($realW / $arr['w']);
        }

        $extension = UploadFile::file_extension($dir.$name);
        switch ($extension) {
            case "gif": $img = @imagecreatefromgif($dir.$name);
            break;
            case "jpg": $img = @imagecreatefromjpeg($dir.$name);
            break;
            case "JPG": $img = @imagecreatefromjpeg($dir.$name);
            break;
            case "jpeg": $img = @imagecreatefromjpeg($dir.$name);
            break;
            case"png": $img = @imagecreatefrompng($dir.$name);
            break;
        }
        $out = @imagecreatetruecolor($arr['w'], $arr['h']);
        @imagealphablending($out, false);
        @imagesavealpha($out, true);
        @imagecopyresampled(
            $out,
            $img,
            0 - ($W - $arr['w']),
            0 - ($H - $arr['h']),
            0,
            0,
            $W,
            $H, 
            $realW,
            $realH);

        switch ($extension) {
            case "gif": @imagegif($out, $dir.$name);
            break;
            case "jpg": @imagejpeg($out, $dir.$name, 85);
            break;
            case "JPG": @imagejpeg($out, $dir.$name, 85);
            break;
            case "jpeg": @imagejpeg($out, $dir.$name, 85);
            break;
            case"png": @imagepng($out, $dir.$name);
            break;
        }
        return $name;
    }

    public function file_extension($filename){
        $path_info = @pathinfo($filename);
        return $path_info['extension'];
    }
}

?>
