<?php
class SqLite{

    protected static $connect;
    protected static $statement;

    function __construct(){
        self::$connect = new SQLite3(_CONFIG_['_DB_']['_HOST_LTE_']._CONFIG_['_DB_']['_NAME_LTE_']);
        if (mysqli_connect_errno()) {
            die(Language::translate()['QueryError2'] . mysqli_connect_error());
        }
    }

    function query($sql){
        if ($result = self::$connect->query($sql)) {
            self::$statement = $result;
        } else {
            if(_CONFIG_['_DB_']['_ERR_QUERY_']==true){
                die(Language::translate()['QueryError1'] .': '.$sql.' -> ' .self::$connect->lastErrorMsg());
            }
        }
    }

    function exec($sql){
        if ($result = self::$connect->exec($sql)) {
            self::$statement = $result;
        } else {
            if(_CONFIG_['_DB_']['_ERR_QUERY_']==true){
                die(Language::translate()['QueryError1'] .': '.$sql.' -> ' .self::$connect->lastErrorMsg());
            }
        }
    }

    function fetch($type='assoc'){
        $type = strtolower($type);
        $statement = self::$statement;
        if($statement != null){
            if($type == 'row' ){
                return $statement->fetchArray(SQLITE3_NUM);
            } elseif($type == 'assoc') {
                return $statement->fetchArray(SQLITE3_ASSOC);
            } elseif($type == 'array') {
                return $statement->fetchArray(SQLITE3_ASSOC);
            }
        }
    }

    function n_rows($table){
        self::query("SELECT COUNT(*) as n FROM {$table}");
        $row = self::fetch();
        $num = $row['n'];
        return $num;
    }

    function get_last_id() {
        return self::$connect->lastInsertRowID();
    }

    function limit($limit='0,'._CONFIG_['_PAGINAZIONE_10_']){
        return " LIMIT {$limit}";
    }

    public function truncate($table, $restart_sequence=1) {
        self::exec("DELETE FROM {$table} ");
        self::exec("DELETE FROM sqlite_sequence WHERE name ='{$table}'");
    }

    public function drop($table) {
        self::exec("DROP TABLE IF EXISTS {$table}");
    }

    function result($type = false) {
        $R = array();
        if($type) $type = strtolower($type);
        else $type = 'assoc';
        while($X = self::fetch($type)) {
            $R[] = $X;
        }
        return $R;
    }

    function __destruct(){
        mysqli_close(self::$connect);
    }
}
?>
