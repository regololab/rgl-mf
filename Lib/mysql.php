<?php

class MySql{

    protected static $connect;
    protected static $statement;

    function __construct(){
        self::$connect = mysqli_connect(
                _CONFIG_['_DB_']['_HOST_MY_'],
                _CONFIG_['_DB_']['_USER_MY_'],
                _CONFIG_['_DB_']['_PASS_MY_'],
                _CONFIG_['_DB_']['_NAME_MY_']
            );
        if (mysqli_connect_errno()) {
            die(Language::translate()['QueryError2'] . mysqli_connect_error());
        }
    }

    function query($sql){
        if ($result = self::$connect->query($sql)) {
            self::$statement = $result;
        } else {
            if(_CONFIG_['_DB_']['_ERR_QUERY_']==true){
                die(Language::translate()['QueryError1'] .': '.$sql.' -> ' .self::$connect->error);
            }
        }
    }

    function fetch($type='assoc'){
        $type = strtolower($type);
        $statement = self::$statement;
        if($statement != null){
            if($type == 'row' ){
                return $statement->fetch_row();
            } elseif($type == 'assoc') {
                return $statement->fetch_assoc();
            } elseif($type == 'obj' || $type == 'object' ) {
                return $statement->fetch_object();
            } elseif($type == 'array') {
                return $statement->fetch_array();
            }
        }
    }

    function n_rows(){
        $row = self::$statement->num_rows;
        return $row;
    }

    function get_last_id() {
        return self::$connect->insert_id;
    }

    function limit($limit='0,'._CONFIG_['_PAGINAZIONE_10_']){
        return " LIMIT {$limit}";
    }

    public function truncate($table, $restart_sequence=1) {
        self::query("TRUNCATE TABLE {$table} ");
        self::query("ALTER TABLE {$table} AUTO_INCREMENT = 1");
    }

    public function drop($table) {
        self::query("DROP TABLE IF EXISTS {$table}");
    }

    function result($type = false) {
        $R = array();
        if($type) $type = strtolower($type);
        else $type = 'assoc';
        while($X = self::fetch($type)) {
            $R[] = $X;
        }
        return $R;
    }

    function __destruct(){
        mysqli_close(self::$connect);
    }
}


 ?>
