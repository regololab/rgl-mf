<?php
// classe per leggere e gestire gli errori
class Avvisi extends Session{
    // gestisce i messaggi di errore
    public function message($message, $template=false) {
        Session::create_session('_MESSAGE_', array(trim($message), $template));
        return false;
    }

    public function print_message() {
        if($_SESSION['_MESSAGE_']) {
            $message = $_SESSION['_MESSAGE_'][0]; // messaggio di errore
            $template_msg = ($_SESSION['_MESSAGE_'][1]) ? ($_SESSION['_MESSAGE_'][1]) : ('ok.php'); // recupera il template del messaggio...
            Session::clear_session('_MESSAGE_');// cancello la sessione che contiene il messaggio di errore

            $files = file_get_contents(_CONFIG_['_ROOT_'].'Templates/message/'.$template_msg);
            $files = str_replace('{{:MESSAGE:}}',$message, $files);
            return $files;
        }
    }

}
/* funzioni
inserire all'interno delle funzioni o classi la generazione del messaggio eg.: LOGIN
Avvisi::message('Messaggio da inserire', 'error.php'); -> error.php, error2.php, warning.php, ok.php

inserire all'interno del template:
echo Avvisi::print_message();
*/

?>
