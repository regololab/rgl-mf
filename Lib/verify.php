<?php
/* DESCRIZIONE CLASSE
 * VeryfySend::GestioneSend($array,$request=false,$email=false);
 * 1: $_POST, $_GET, $_REQUEST... l'importante che sia un array di variabile globale
 * 2: se esiste creare un array con le voci che si vuole far rendere obbligatorie
 *    es: array('nome','username','password') ad esempio di un dato $_POST['nome'], $_POST['username'], $_POST['password']
 *    la variabile sara contenuta nella prima variabile ($array)
 * 3: verifico se un indirizzo email inserito sia corretto. Inseriro il nome della variabile ad esempio $_POST['Email'] -> Email
 *
 * Esempio Concreto :
 *  $controllo = VeryfySend::GestioneSend($_POST, array('username','password'), 'username'); # username = xyz@xyz.it
 *  if(!$controllo){return false;}
    if($controllo==1){Avvisi::message('Attenzione i campi contrassegnati da * sono Obbligatori','template.php');return false;}
    if($controllo==2){Avvisi::message('Inserire un indirizzo email valido','template.php');return false;}
*/
class VerifySend{

    function GestioneSend($array, $request=false, $email=false) {
        if($array){
            if(is_array($request)){
                foreach($request as $V){
                    if(!preg_match('~^!~', $V)){
                        if(!$array[$V]) return 1;
                    }
                }
            }
            if($email){
                $mail = self::email($array[$email]);
                if(!$mail) return 2;
            }
            return $array;
        } else {
            return false;
        }
    }

    function email($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }

}

?>
