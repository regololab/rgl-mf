<?php
class BreadC {

    // restituisce il percorso relativo ad esempio ../../ rispetto alla posizione di partenza
    public function position_relative() {
        if(self::bc_this_page() == '/') return './';
        $page = explode('/', self::ext_path());
        $n_page = count($page)-1;
        $dir = '';
        if($n_page == 0) return './';

        for($i = 0; $i < $n_page; $i++){
            $dir .='../';
        }
        return $dir;
    }

    // creo le briciole di pane in array con nome valore e indirizzo
    public function bread_crumbs() {
        $page = explode('/', self::ext_path());
        $n_page = count($page) -1;
        $pages = array();
        $pagex = '';
        $pages[-1]['index'] = (self::bc_this_page() == $page[$i].'/') ? (1) : (0);
        $pages[-1]['name'] = 'Home';
        $pages[-1]['link'] = _CONFIG_['_SITE_'];
        for($i = 0; $i < $n_page; $i++){
            $pagex = ($pagex) ? ($pagex) : ('');
            $pagina = $pagex.$page[$i].'/';
            $pagina = str_replace('//','/',$pagina);
            $pages[$i]['index'] = (self::bc_this_page() == $page[$i].'/') ? (1) : (0);
            $pages[$i]['name'] = ucwords(str_replace(array('_','-'), ' ', $page[$i]));
            $pages[$i]['link'] = _CONFIG_['_SITE_'].$pagina;
            $pagex .= $page[$i].'/';
        }
        return $pages;
    }

    // estrae tutto il percorso dalla cartella di root alla posizione attuale ex: (escluso /var/www/) dir1/dir2/dir3/
    public function ext_path() {
        $page = explode('?', htmlspecialchars($_SERVER['REQUEST_URI']));
        if(_CONFIG_['_HOME_'] != '/'){
            $address = str_replace(_CONFIG_['_HOME_'], '', $page[0]);
        } else {
            $address = $page[0];
        }

        $address = explode('/', $address);
        $address = array_filter($address, 'strlen');
        $pages = array();
        foreach($address as $Vadd) {
            if(!preg_match('@\.@', $Vadd)){
                if($Vadd){
                    $pages[] = ($Vadd) ? (_CONFIG_['_HOME_'].$Vadd.'/') : ('');
                }
            }
        }
        $Key = array_keys($pages, _CONFIG_['_HOME_']);
        $pagex = '';
        foreach($pages as $K=>$V){
            if(_CONFIG_['_HOME_'] == '/') {
                if($K >= $Key[0]){
                    $pagex .= $V;
                }
            } else {
                if($K >= isset($Key[0])){
                    $pagex .= str_replace(_CONFIG_['_HOME_'], '', $V);
                }
            }
        }
        return str_replace('//','/',$pagex);
    }

    public function bc_this_page() {
        $page = explode('/', self::ext_path());
        $page = array_reverse($page);
        return $page[1].'/';
    }

}

?>
