<?php
class Files {

    public function createFile($file) {
        $f = fopen($file,"w");
        fwrite($f,'');
        fclose($f);
    }

    public function writeNewFile($file, $text) {
        $f = fopen($file,"w");
        fwrite($f,$text."\r\n");
        fclose($f);
    }

    public function addStartTextFile($file, $text) {
        $textb = self::readFile($file, 1);
        $f = fopen($file,"r+");
        $text = $text."\r\n".$textb;
        fwrite($f,$text);
        fclose($f);
    }

    public function addEndTextFile($file, $text) {
        $f = fopen($file,"a");
        fwrite($f,$text."\r\n");
        fclose($f);
    }

    public function readFile($file, $type=false) {
        if($type){ // se il type (true) esiste, mostro il file testuale
            $files = '';
            $files = file_get_contents($file);
        } else { // altrimenti mostro il contenuto diviso in array riga per riga
            $files = array();
            $files = file($file);
        }
        return $files;
    }


    public function extensionFile($file) {
        $pathInfo = pathinfo($file);
        return $pathInfo['extension'];
    }

    // lettura file presenti in una directory
    public function getDir($dir) {
        if (is_dir($dir)) {
            $files = array();
            if ($directory = opendir($dir)) {
                while (($file = readdir($directory)) !== false) {
                    if((!is_dir($file)) && ($file != ".") && ( $file != "..")) {
                        $files[] = $file;
                    }
                }
                closedir($directory);
                return $files;
            }
        }
    }

}
?>
