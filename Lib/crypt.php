<?php
class Crypto {
	// funzione simple crypt & decrypt dati get
	function Crypt($dato) {
		$dati=base64_encode(convert_uuencode($dato));
		$frase1=str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-");
		$frase2=str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-");
		$frase1=substr($frase1,0,3);
		$frase2=substr($frase2,0,3);
		$stringa=$frase1.$dati.$frase2;
		$string=str_split($stringa);
		ksort($string);

		foreach($string AS $V)	{
			$frases=str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
			$str.=$frases{1}.ord($V);
		}
		// nuova gestione di cryptazione
		$key = hash('sha256', _CONFIG_['ENCRYPTION_KEY']);
		$iv = substr(hash('sha256', _CONFIG_['ENCRYPTION_IV']), 0, 16);
		$str = openssl_encrypt($str, _CONFIG_['ENCRYPTION_METHOD'], $key, 0, $iv);
		$str = base64_encode($str);
		return $str;
	}

	function Decrypt($dato){
		$key = hash('sha256', _CONFIG_['ENCRYPTION_KEY']);
		$iv = substr(hash('sha256', _CONFIG_['ENCRYPTION_IV']), 0, 16);
		$dato = openssl_decrypt(base64_decode($dato), _CONFIG_['ENCRYPTION_METHOD'], $key, 0, $iv);
		$dati=preg_replace("/[A-Z]/i","-",$dato);
		$dati=preg_replace("/^-/","",$dati);
		$my_dato=explode("-",$dati);
		ksort($my_dato);

		foreach($my_dato AS $V)	{
			$str.=chr($V);
		}
		$derypt=substr($str,3);
		$derypt=substr($derypt,0,-3);
		$derypt=convert_uudecode(base64_decode($derypt));
		return $derypt;
	}

	function gen_pass($pass){
		$pass = sha1(md5(md5(sha1($pass))));
		return $pass;
	}
}

?>
