<?php
class Language{

    function sel_language() {
        $lang=$_GET['lang'];
        if(strlen($lang) == '2'){
            session_start();
            $_SESSION['Lang'] = $lang;
            return $lang;
        } else {
            if(!$_SESSION['Lang']){
                session_start();
                $_SESSION['Lang'] = _CONFIG_['_DEFAULT_LANG_'];
                return _CONFIG_['_DEFAULT_LANG_'];
            } else {
                return $_SESSION['Lang'];
            }
        }
    }

    function view_language(){
        $lang = $_SESSION['Lang'];
        // seleziona immagine lingua default
        $sql = "SELECT * FROM "._CONFIG_['_DB_']['_PREFIX_']."language WHERE code = '{$lang}' ".DB::limit('0,1');
        DB::Query($sql);
        $R = DB::result();
        // Seleziona tutte le lingue Disponibili
        $sql = "SELECT * FROM "._CONFIG_['_DB_']['_PREFIX_']."language WHERE active='1' ORDER BY code ASC ";
        DB::Query($sql);
        $Res = DB::result();
        return array('lingua_default'=>$R, 'lingue'=>$Res);
    }

    # Gestione lingua ####
    function lang(){
        $Lang = self::sel_language();
        $view_language = self::view_language();
        return array(
            'Lang'=>$Lang,
            'View_language'=>$view_language,
        );
    }

    function translate(){
        $json_data = file_get_contents(_CONFIG_['_LANG_'].$_SESSION['Lang'].'.json');
        $_Translate = json_decode($json_data, true);
        return $_Translate;
    }
}

 ?>
