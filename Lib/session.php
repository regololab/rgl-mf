<?php
class Session {
	// creo una nuova sessione.
	public function create_session($nome, $value){
		$val=array('.','-',';',':',' ','  ','   ');
		$change=array('_','_','_','_','_','_','_');
		$nome=str_replace($val,$change,trim($nome)); // trasformo i separatori usati nel nome in "_" under score
		@session_start(); // apro la sessione
		$_SESSION[$nome]=$value; // inserisco il valore nella sessione
		@session_write_close(); // chiudo la sessione
	}

	public function clear_session($nome) { // cancello le sessioni che non mi servono piu.
		$val=array('.','-',';',':');
		$change=array(',',',',',',',');
		// trasformo i separatori usati nel nome in "," per poi spezzarlo e cancellare piu variabili
		$nome=str_replace($val,$change,trim($nome));
		$NameSess=explode(',',$nome); //spezzo i nomi
		@session_start(); // apro la sessione
		foreach($NameSess AS $V) {
			if($_SESSION[trim($V)]) { // verifico che esista questa sessione
				unset($_SESSION[trim($V)]); // cancello la sessione
			}
		}
		@session_write_close(); // chiudo la sessione
	}
}


?>
