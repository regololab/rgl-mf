<?php
class DTime {

    #DTime::data_info('Y-m-d', $data); simple mode
    #DTime::data_info('Y-m-d', $data, array('separator', 'd', 'm', 'Y')); return data mode
    function data_info($format, $data, $arr=0){
        $date = DateTime::createFromFormat($format, $data);
        if($arr){
            $n_arr = count($arr);
            $separator = $arr[0];
            $data = array();
            for($i = 1; $i < $n_arr; $i++){
                $data[]= $date->format($arr[$i]);
            }
            return implode($data, $separator);
        } else {
            return array(
                'Y'=>$date->format('Y'), # Una rappresentazione numerica completa di un anno, 4 cifre : 2015
                'y'=>$date->format('y'), # Una rappresentazione a due cifre di un anno : 99 o 03
                'm'=>$date->format('m'), # Rappresentazione numerica di un mese, con gli zero iniziali : 01 a 12
                'd'=>$date->format('d'), # Giorno del mese, 2 cifre con zero iniziali : 01 a 31
                'w'=>$date->format('w'), # Rappresentazione numerica del giorno della settimana : 0 (per Domenica) a 6 (per Sabato)
                'N'=>$date->format('N'), # Rappresentazione numerica ISO-8601 del giorno della settimana : 1 (per Luned�) a 7  (per Domenica)
                'H'=>$date->format('H'), # Formato a 24 ore di un'ora con gli zero iniziali : 00 a 23
                'i'=>$date->format('i'), # Minuti con gli zero iniziali : 00 a 59
                's'=>$date->format('s'), # Secondi, con gli zero iniziali : 00 a 59
                'P'=>$date->format('P'), # Differenza dall'ora di Greenwich (GMT) con due punti tra ore e minuti : Esempio: +02:00
                'U'=>$date->format('U'), # Il numero di secondi dalla Unix Epoch : time()
            );
        }
    }

    # General::ztime('Y-m-d H:i:s')['time']
    # General::ztime('Y-m-d')['date'];
    # General::ztime('H:i')['date'];
    # General::ztime('Y-m-d', '2017-02-15')['date'];
    # General::ztime('w', '2017-02-14')['date'];
    # General::ztime('d/m/Y H:i:s')['date'];
    function ztime($format, $data=0){
        $data = ($data) ? ($data) : (date());
        $time_zone = ($_SESSION['time_zone']) ? ($_SESSION['time_zone']) : (_CONFIG_['_TIME_ZONE_']);
        $date = date_create($data, timezone_open($time_zone));
        $time_zone = date_format($date, $format);
        $time = strtotime($time_zone);
        return array(
            'date'=>$time_zone,
            'time'=>$time,
        );
    }

    function list_ztime(){
        $list = array();
        foreach(timezone_abbreviations_list() as $abbr => $timezone){
            foreach($timezone as $val){
                if(isset($val['timezone_id'])){
                    $list[] = $val['timezone_id'];
                }
            }
        }
        return $list;
    }

    // inseriti 2 valori data calcola il numero di giorni che li separa (How many days)
    public function hmd($data1,$data2) {
        if($data1 && $data2) {
            $datax=strtotime($data1);
            $datay=strtotime($data2);
            $ndata=($datay-$datax);
            if($ndata==0) {
                return 1;
            } else {
                if($ndata > 0) {
                    return ($ndata/86400)+1;
                } else {
                    return ($ndata/86400)-1;
                }
            }
        }
    }
}

?>
