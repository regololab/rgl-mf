<?php
class Str{

    // pulizia della stringa per inserimento db
    public function str_clear($stringa,$html=false,$ajax=false,$tipo_db=_CONFIG_['_DB_']['_TIPO_DB_']) {
        if($html) $stringa = strip_tags($stringa);

        $stringa = str_replace("\\","", $stringa);
        $stringa = trim($stringa);
        if($ajax) {
            $stringa = urldecode($stringa);
            $stringa = utf8_decode($stringa);
        }
        if($tipo_db == "MYSQL") {
            $stringa = addslashes($stringa);
        } else {
            $stringa = SQLite3::escapeString($stringa);
        }
        return $stringa;
    }

    // serve (soprattutto) per testo con caratteri speciali e virgolettato inserito all'interno di codice javascript
    public function js_clean($string) {
        $stringa = addslashes($string);
        $stringa = htmlspecialchars($stringa, ENT_QUOTES);
        return $stringa;
    }

    // serve per selezionare la parte di una frase in base al numero di caratteri
    public function w_count($s, $n, $end_point='') {
        $length = strlen($s);
        $string = explode(" ", trim($s));
        $str = array();
        $n_str = 0;
        foreach($string as $V){
            $n_str += strlen($V);
            if($n_str <= $n){
                $str[] = $V;
            } else {
                break;
            }
        }
        if($length > $n){
            return implode(" ", $str).$end_point;
        } else {
            return implode(" ", $str);
        }
    }

    // serve a generare un link
    public function gen_url($str, $delimiter='-') {
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[_|+ -]+/", $delimiter, $clean);
        return $clean;
    }
}

?>
