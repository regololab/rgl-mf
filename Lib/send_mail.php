<?php
class SendMail extends \Avvisi {

    const TypeFile = array('pdf', 'jpg', 'gif');
    protected static $input_name;

    public function text_mail($to, $subject, $from, $text, $Cc=false, $Bcc=false) {
        if($to && $subject){
            $header = "From: <".$from.">\r\n";
            if($Cc){
               $header .= "cc: ".$Cc."\r\n";
            }
            if($Bcc){
               $header .= "Bcc: ".$Bcc."\r\n";
            }
            $header .= 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/plain; charset=UTF-8' . "\r\n";
            $header .= 'X-Mailer: PHP/' . phpversion();
            $email = @mail($to, $subject, $text, $header);
            if($email == true) {
                \Avvisi::message(Language::translate()['ok_email'], 'ok.php');
            } else {
                \Avvisi::message(Language::translate()['problem_mail'], 'warning.php');
            }
        }
    }

    public function html_mail($to, $subject, $from, $text, $Cc=false, $Bcc=false) {
        if($to && $subject){
            $header  = 'MIME-Version: 1.0' . "\r\n";
            $header .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $header .= "From: <".$from.">\r\n";
            if($Cc){
               $header .= "cc: ".$Cc."\r\n";
            }
            if($Bcc){
               $header .= "Bcc: ".$Bcc."\r\n";
            }
            $header .= 'X-Mailer: PHP/' . phpversion();
            $email = @mail($to, $subject, $text, $header);
            if($email == true) {
                \Avvisi::message(Language::translate()['ok_email'], 'ok.php');
            } else {
                \Avvisi::message(Language::translate()['problem_mail'], 'warning.php');
            }
        }
    }

    public function attach_mail($name_input, $to, $subject, $from, $text, $Cc=false, $Bcc=false) {
        if($name_input && $to && $subject){
            self::$input_name = $name_input; // nome inviato tramite campo file input

            $arrayMail = array(
                'to'=>$to,
                'subject'=>$subject,
                'from'=>$from,
                'text'=>$text,
                'Cc'=>$Cc,
                'Bcc'=>$Bcc,
            );

            // qui estrapolo i dati per compilarli correttamente
            $Compilatore = self::GestioneAllegato($arrayMail);
            $extension = $Compilatore['tipo'];// tipo file (estensione)
            $upload = $Compilatore['upload'];// verifico se c'e un file da allegare
            $message = $Compilatore['testo'];// inserisco il testo
            // se non esiste l'allegato invio comunque l'email in html
            // commentare questa sezione se non si vuole tale funzionalita
            if(!$Compilatore['tipo']){
                self::html_mail($arrayMail['to'], $arrayMail['subject'], $arrayMail['from'], $arrayMail['text'], $arrayMail['Cc'], $arrayMail['Bcc']);
                return false;
            }

            if($upload == 1) { // qui verifico se l'estensione e' ex: .pdf
                if(preg_match("/^([a-z0-9_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,4}\$/i", $arrayMail['to']) && preg_match("/^([a-z0-9_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,4}\$/i", $arrayMail['from'])) {
                    $sendMail = @mail($arrayMail['to'], $arrayMail['subject'], "", $Compilatore['header']);
                    if($sendMail == true) {
                        \Avvisi::message(Language::translate()['ok_email'], 'ok.php');
                    } else {
                        \Avvisi::message(Language::translate()['problem_mail'], 'warning.php');
                    }
                } else {
                    \Avvisi::message(Language::translate()['invalid_email'], 'warning.php');
                }
            } elseif($upload == 2) {
                \Avvisi::message(Language::translate()['invalid_file_email'], 'warning.php');
            }
        }
    }

    public function email_attach_local_file($file, $file_name, $to, $subject, $from, $text, $Cc=false, $Bcc=false){
        $file = $file;
        $filename = $file_name;

        $mailto = $to;
        $subject = $subject;
        $message = $text;

        $content = file_get_contents($file);
        $content = chunk_split(base64_encode($content));
        $separator = md5(time());

        $headers = "From: <{$from}>\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"\r\n";
        $headers .= "Content-Transfer-Encoding: 7bit\r\n";
        $headers .= "This is a MIME encoded message.\r\n";
        if($Cc){
           $headers .="cc: ".$Cc."\r\n";
        }
        if($Bcc){
           $headers .="Bcc: ".$Bcc."\r\n";
        }

        $body = "--".$separator."\r\n";
        $body .= "Content-Type: text/html; charset=utf-8\r\n";
        $body .= "Content-Transfer-Encoding: 8bit\r\n";
        $body .= $message."\r\n";

        $body .= "--".$separator."\r\n";
        $body .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n";
        $body .= "Content-Transfer-Encoding: base64\r\n";
        $body .= "Content-Disposition: attachment\r\n";
        $body .= $content."\r\n";
        $body .= "--".$separator."--";

        if (mail($mailto, $subject, $body, $headers)) {
            \Avvisi::message(Language::translate()['ok_email'], 'ok.php');
        } else {
            \Avvisi::message(Language::translate()['problem_mail'], 'warning.php');
        }
    }

    private function GestioneAllegato($array) {
        $file = $_FILES[self::$input_name]['tmp_name'];// estrapolo il nome temporaneo del file
		$type = $_FILES[self::$input_name]['type']; // verifico il tipo di file
		$file_name = $_FILES[self::$input_name]['name']; // prelevo il nome del file
		$file_size = $_FILES[self::$input_name]['size']; // prelevo il peso
		$path_info = @pathinfo($file_name); // rilevo estensione
		$path_inf=$path_info['extension']; // estensione pulita es: pdf

		// qui tento di inviare una email con allegato
        // verifico che esista un file e controllo se l'estensione e' quella consentita
		if ($file_name && in_array($path_inf, self::TypeFile)) {
			$handle = @fopen($file, "rb");
			$content = @fread($handle, $file_size);
			@fclose($handle);
			$content = @chunk_split(base64_encode($content));
			$uid = md5(uniqid(time()));

			$header = "From: <".$array['from'].">\r\n";
            if($array['Cc']) $header.= "Cc: ".$array['Cc']."\r\n";
            if($array['Bcc']) $header.= "Bcc: ".$array['Bcc']."\r\n";
			$header .= "MIME-Version: 1.0\r\n";
			$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
			$header .= "This is a multi-part message in MIME format.\r\n";
			$header .= "--".$uid."\r\n";
			$header .= "Content-type:text/html; charset=utf-8\r\n";
			$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
			$header .= $array['text']."\r\n\r\n";
			$header .= "--".$uid."\r\n";
			$header .= "Content-Type: application/octet-stream; name=\"".$file_name."\"\r\n"; // use different content types here
			$header .= "Content-Transfer-Encoding: base64\r\n";
			$header .= "Content-Disposition: attachment; filename=\"".$file_name."\"\r\n\r\n";
			$header .= $content."\r\n\r\n";
			$header .= "--".$uid."--";
			$txt=array(
                'tipo' => $path_inf,
                'header' => $header,
                'testo' => '',
                'upload' => 1
            );
		} else { // qui l'allegato non e' corretto
			$txt=array(
                'tipo' => $path_inf,
                'header' => '',
                'testo' => '',
                'upload' => 2
            );
		}
		return $txt;
    }

}

?>
