<?php
class KuiperBelt {

    public function __construct($kuiper) {
        $this->kuiper = $kuiper;
        $this->pages = array('.html','.htm','.php','.asp','.txt','.json','.css','.js','.xml');
    }

    public function get_template($template, $Rgl, $_){
        require_once(_CONFIG_['_ROOT_'].'Templates/'.$template);
    }

    public function kuiperBelt() {
        $pages = self::StripAddress();
        $page = explode('?', $pages);
        $pages = preg_replace('@\/$@', '', $page[0]);
        $address = explode('/', $pages);
        $add = array_reverse($address);
        $add = self::StripPhp($add);

        /* se gestisco i file tramite htaccess - utile in front end*/
        if(self::inAnnidateArray($add[0], $this->kuiper)){
            $name = self::componeName($add[0]);
            $nameClass = self::generateNameClass($name.'Model');
            require_once(_CONFIG_['_ROOT_'].'Model/'.$name.'Model.php');
            require_once(_CONFIG_['_ROOT_'].'View/'.$name.'View.php');
            return false;
        } elseif(self::inAnnidateArray($add[count($add)-1], $this->kuiper)){
            $name = self::componeName($add[count($add)-1]);
            $nameClass = self::generateNameClass($name.'Model');
            require_once(_CONFIG_['_ROOT_'].'Model/'.$name.'Model.php');
            require_once(_CONFIG_['_ROOT_'].'View/'.$name.'View.php');
            return false;
        }
        # altrimenti eseguo le pagine in htaccess
        $nameClass = self::generateNameClass('indexModel');
        require_once(_CONFIG_['_ROOT_'].'Model/'.'indexModel.php'); #Model
        require_once(_CONFIG_['_ROOT_'].'View/'.'indexView.php'); #View
        return false;
        /* FINE */

    }

    private function StripPhp($arr){
        foreach($arr as $K=>$V) {
            foreach($this->pages as $Vpage){
                if(preg_match('@'.$Vpage.'@',$V) || !$V){
                    unset($arr[$K]);
                }
            }
        }
        sort($arr, SORT_NUMERIC);
        return $arr;
    }

    private function StripAddress() {
        $pages = explode(_CONFIG_['_HOME_'], htmlspecialchars($_SERVER['REQUEST_URI']));
        $pagex = array();
        foreach($pages as $V){
            if($V){
                $pagex[] = $V;
            }
        }
        $pagex = array_reverse($pagex);
        return preg_replace('@^\/$@', '', $pagex[0]);
    }

    private function generateNameClass($controller) {
        $controller = str_replace('.php', '', $controller);
        $controller = ucfirst($controller);
        return $controller;
    }

    private function componeName($name) {
        $name = str_replace(array('-','_','/'), '@', $name);
        $nameC = explode('@', $name);
        $theName = '';
        foreach($nameC as $K=>$V){
            if($K == 0){
                $theName .= $V;
            } else {
                $theName .= ucfirst($V);
            }
        }
        return $theName;
    }

    private function inAnnidateArray($named, $array, $strict = false){
        foreach ($array as $item) {
            if (($strict ? $item === $named : $item == $named) || (is_array($item) && self::inAnnidateArray($named, $item, $strict))) {
                return true;
            }
        }
        return false;
    }
}

?>
