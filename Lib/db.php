<?php

class DB {

    protected static $connect;
    protected static $statement;

    public function Connect() {
        # mysql
        if(_CONFIG_['_DB_']['_TIPO_DB_'] == "MYSQL")  {
            $str = 'mysql:host='._CONFIG_['_DB_']['_HOST_DB_'].';dbname='._CONFIG_['_DB_']['_NAME_DB_'].'';
            try {
                self::$connect = new PDO($str, _CONFIG_['_DB_']['_USER_DB_'], _CONFIG_['_DB_']['_PASS_DB_']);
                return $connect = self::$connect;
            } catch(PDOException $e) {
                die ('Error connession: '.$e->getMessage());
            }
        }

        # sqlite
        elseif(_CONFIG_['_DB_']['_TIPO_DB_'] == "SQLITE")  {
            $str = 'sqlite:'._CONFIG_['_DB_']['_HOST_DB_']._CONFIG_['_DB_']['_NAME_DB_'].'';
            try {
                self::$connect = new PDO($str);
                return $connect = self::$connect;
            } catch(PDOException $e) {
                die ('Error connession: '.$e->getMessage());
            }
        }

        # pgsql
        elseif(_CONFIG_['_DB_']['_TIPO_DB_'] == "PGSQL")  {
            $str = 'pgsql:dbname='._CONFIG_['_DB_']['_NAME_DB_'].';host='._CONFIG_['_DB_']['_HOST_DB_'].'';
            try {
                self::$connect = new PDO($str, _CONFIG_['_DB_']['_USER_DB_'], _CONFIG_['_DB_']['_PASS_DB_']);

                return $connect = self::$connect;
            } catch(PDOException $e) {
                die ('Error connession: '.$e->getMessage());
            }
        }
    }

    public function query($string, $param = false, $error = false) {
        if($statement = DB::Connect()->prepare($string)){
            if(is_array($param)){
                foreach($param as $K=>$V){
                    if(gettype($V) == "boolean") $PDO_PARAM = PDO::PARAM_BOOL;
                    elseif(gettype($V) == "integer") $PDO_PARAM = PDO::PARAM_INT;
                    elseif(gettype($V) == "double") $PDO_PARAM = PDO::PARAM_FLOAT;
                    elseif(gettype($V) == "string") $PDO_PARAM = PDO::PARAM_STR;
                    elseif(gettype($V) == "NULL") $PDO_PARAM = PDO::PARAM_NULL;
                    $statement->bindValue($K, $V, $PDO_PARAM);
                }
            }
            $stato = $statement->execute();
            if(_CONFIG_['_DB_']['_ERR_QUERY'] && !$stato){
                die($error." -> ".$string.'<br/>');
            }
            self::$statement = $statement;
        }
    }

    public function fetch($type) {
        $type = strtolower($type);
        $statement = self::$statement;
        if($statement != null){
            if($type == 'row' ){
                return $statement->fetch(PDO::FETCH_NUM);
            } elseif($type == 'assoc') {
                return $statement->fetch(PDO::FETCH_ASSOC);
            } elseif($type == 'obj' || $type == 'object' ) {
                return $statement->fetch(PDO::FETCH_OBJ);
            } elseif($type == 'array') {
                return $statement->fetch(PDO::FETCH_BOTH);
            }
        } else {
            if(_CONFIG_['_DB_']['_ERR_QUERY_']==true){
                die(Language::translate()['QueryError1']);
            } else {
                return false;
            }
        }
    }


    public function result($type = false) {
        $R = array();
        if($type) $type = strtolower($type);
        else $type = 'assoc';
        while($X = DB::fetch($type)) {
            $R[] = $X;
        }
        return $R;
    }

    public function n_table_rows($table, $array=false) {
        $count = ($array['count']) ? ($array['count']) : ('*');
        $where = ($array['where']) ? (' WHERE '.$array['where']) : ('');
        $sql = "SELECT COUNT(".$count.") FROM ".$table." ".$where."";
        $n_rows = DB::Connect()->query($sql)->fetchColumn();
        return $n_rows;
    }

    public function get_last_id($columns) {
        return self::$connect->lastInsertId($columns);
    }

    public function limit($limit) {
        $num = explode(',', $limit);
        if(_CONFIG_['_DB_']['_TIPO_DB_'] == "MYSQL")  {
            $limit = (!$num[1]) ? (" LIMIT ".$num[0]." ") : (" LIMIT ".$num[0].",".$num[1]." ");
        } elseif(_CONFIG_['_DB_']['_TIPO_DB_'] == "SQLITE")  {
            $limit = (!$num[1]) ? (" LIMIT ".$num[0]." ") : (" LIMIT ".$num[0].",".$num[1]." ");
        } elseif(_CONFIG_['_DB_']['_TIPO_DB_'] == "PGSQL")  {
            $limit = (!$num[1]) ? (" LIMIT ".$num[0]." ") : (" LIMIT ".$num[1]." OFFSET ".$num[0]." ");
        }
        return $limit;
    }

    public function truncate($table, $restart_sequence=1) {
        if(_CONFIG_['_DB_']['_TIPO_DB_']=='PGSQL'){
            $db = self::Connect();
            if($restart_sequence){
                $rs = $db->exec("TRUNCATE TABLE {$table} RESTART IDENTITY");
            } else {
                $rs = $db->exec("TRUNCATE TABLE {$table} ");
            }
        } elseif(_CONFIG_['_DB_']['_TIPO_DB_']=='MYSQL') {
            $rs = $db->exec("TRUNCATE FROM {$table}");
        } elseif(_CONFIG_['_DB_']['_TIPO_DB_']=='SQLITE') {
            $db = self::Connect();
            $rs = $db->exec("DELETE FROM {$table}");
            if($restart_sequence){
                $rs = $db->exec("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '{$table}'");
            }
        }
    }

    public function drop($table) {
        $db = self::Connect();
        $rs = $db->exec("DROP TABLE {$table}");
    }

    public function __destruct() {
        self::$connect = null;
        self::$statement = null;
    }

    public function name_column($name){
        $res = DB::Connect()->query("SELECT * FROM {$name} LIMIT 1");
        for ($i = 0; $i < $res->columnCount(); $i++) {
            $col = $res->getColumnMeta($i);
            $columns[] = $col['name'];
        }
        return $columns;
    }
}

?>
