<?php
include(_CONFIG_['_ROOT_']."Lib/abstract.php");

class Rgl extends Ext {

    public $Lib = array(
            'Crypto' => 'crypt.php',
            'Session' => 'session.php',
            'Language' => 'language.php',
            'Avvisi' => 'avvisi.php',
            'Str' => 'string.php',
            'DTime' => 'dtime.php',
            'VerifySend' => 'verify.php',
            'SendMail' => 'send_mail.php',
            'DB' => 'db.php',
            'MySql' => 'mysql.php',
            'SqLite' => 'sqlite.php',
            'PgSql' => 'postgresql.php',
            'BreadC' => 'breadcrumbs.php',
            'LogIn' => 'log_in.php',
            'Files' => 'work_files.php',
            'Files' => 'work_files.php',
            'UploadFile' => 'upload_file.php',
            'Restful' => 'restful.php',
        );

    public function __construct($class) {
        foreach($class as $V){
            include_once(_CONFIG_['_ROOT_'].'Lib/'.$this->Lib[$V]);
            parent::addExt(new $V());
        }
    }

    # genero tabelle DB ###
    function generate_table($json){
        return $json;
    }

    # percorsi e pagine ####
    function this_page(){
        $myPage = BreadC::ext_path();
        $relative = BreadC::position_relative();
        return array(
            'myPage'=>$myPage,
            'relative'=>$relative,
        );
    }

    # ritorna indirizzo IP
    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = htmlspecialchars($_SERVER['HTTP_CLIENT_IP']);
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = htmlspecialchars($_SERVER['HTTP_X_FORWARDED_FOR']);
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = htmlspecialchars($_SERVER['HTTP_X_FORWARDED']);
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = htmlspecialchars($_SERVER['HTTP_FORWARDED_FOR']);
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = htmlspecialchars($_SERVER['HTTP_FORWARDED']);
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = htmlspecialchars($_SERVER['REMOTE_ADDR']);
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    # verifica richiesta ajax
    function is_ajax(){
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
            return 1;
        } else {
            header('Location: '._CONFIG_['_ERROR_404_']);
        }
    }

}
?>
