<?php
// classe LOGIN
Class LogIn {

    function this_col(){
        $cols = DB::name_column(_CONFIG_['_TABLE_']);
        $col = implode(", ",$cols);
        return $col;
    }

    function VerificaDati() {
        $verifica = VerifySend::GestioneSend($_POST, array(_CONFIG_['_USER_INPUT_'],_CONFIG_['_PASS_INPUT_']));
        if(!$verifica) return false;
        return $verifica;
    }

    function ControlloDatiDb() {
        $Dati = LogIn::VerificaDati();
        if(!$Dati){return false;}
        $id_col=trim(DB::name_column(_CONFIG_['_TABLE_'])[0]); // nome campo id
        $user_col=trim(DB::name_column(_CONFIG_['_TABLE_'])[1]); // nome campo user
        $pass_col=trim(DB::name_column(_CONFIG_['_TABLE_'])[2]);// nome campo pass
        $stato_col=trim(DB::name_column(_CONFIG_['_TABLE_'])[3]);// nome active
        $sql="SELECT ".self::this_col()." FROM "._CONFIG_['_TABLE_']." WHERE ".$user_col."='".Str::str_clear($Dati[_CONFIG_['_USER_INPUT_']],1)."' AND ".$pass_col."='".Crypto::gen_pass($Dati[_CONFIG_['_PASS_INPUT_']])."' AND ".$stato_col."=1 LIMIT 0,1";
        DB::Query($sql); // query su databese
        $R = DB::fetch('assoc');
        if($R['id'] > 0) {
            foreach(DB::name_column(_CONFIG_['_TABLE_']) as $Vcol){
                if($Vcol != 'password'){
                    Session::create_session($Vcol,$R[$Vcol]);
                }
            }
            Session::create_session(_CONFIG_['_ELW_'],Crypto::Crypt(_CONFIG_['_KYE_']));
            Session::create_session('my_rand',rand(100000,999999).substr(str_shuffle('abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPRSTUVWXYZ'),0,10));
        } else {
            foreach(DB::name_column(_CONFIG_['_TABLE_']) as $Vcol){
                Session::clear_session($Vcol);
            }
            Session::clear_session('my_rand,'._CONFIG_['_ELW_']);
            LogIn::SignOut(1);
        }

    }

    private function VerificaLogin() {
        // verifico che esistano i dati nelle sessioni
        if(!$_SESSION['my_user'] && !$_SESSION[_CONFIG_['_ELW_']] && !$_SESSION['my_id'] && Crypto::Decrypt($_SESSION[_CONFIG_['_ELW_']]) != _CONFIG_['_KYE_']) {
            LogIn::ControlloDatiDb();
            if(!$_SESSION['my_user'] && !$_SESSION[_CONFIG_['_ELW_']] && !$_SESSION['my_id'] && Crypto::Decrypt($_SESSION[_CONFIG_['_ELW_']]) != _CONFIG_['_KYE_']) {
                Avvisi::message(Language::translate()['err_1'], 'error.php');
                return false;
            }
        }
        return true;
    }

    public function logout($var = false) {
        if(preg_match("/\?exit/i", htmlspecialchars($_SERVER['REQUEST_URI'])) || $_GET['exit']==1) { # aggiungere se si ha necessit� -> || $var==1
            foreach(DB::name_column(_CONFIG_['_TABLE_']) as $Vcol){
                Session::clear_session($Vcol);
            }
            Session::clear_session('my_rand,'._CONFIG_['_ELW_']);
            Avvisi::message(Language::translate()['logout'], 'ok.php');
            header("Location: "._CONFIG_['_LOGOUT_']."");
        }
    }


    public function SignOut($var = false) {
        if($var==1) {
            foreach(DB::name_column(_CONFIG_['_TABLE_']) as $Vcol){
                Session::clear_session($Vcol);
            }
            Session::clear_session('my_rand,'._CONFIG_['_ELW_']);
            header("Location: "._CONFIG_['_LOGOUT_']."?er=1");
        }
    }

    public function protection($sezione, $profondita, $redirect=false) {
        $Dati = LogIn::VerificaLogin();
        if(!$Dati){LogIn::SignOut(1);}
        LogIn::Sezione($sezione, $redirect);
        if(is_array($profondita)) {
            $permesso=(in_array($_SESSION[DB::name_column(_CONFIG_['_TABLE_'])[5]], $profondita)) ? (1) : (0);
            if (!$permesso or _CONFIG_['_KYE_'] != Crypto::Decrypt($_SESSION[_CONFIG_['_ELW_']])) {
                LogIn::SignOut(1);
            }
            if(is_array($redirect) && count($redirect) > 0){
                LogIn::Redirect($redirect);
            } else {
                return 1;
            }
        }
    }

    // >> creazione istanza per protezione redirect categoria
    private function Sezione($sezione, $redirect) {
        if($_SESSION[DB::name_column(_CONFIG_['_TABLE_'])[3]] != "1"){
            if($redirect){
                if(!in_array($_SESSION[DB::name_column(_CONFIG_['_TABLE_'])[4]], $sezione)) {
                    foreach(DB::name_column(_CONFIG_['_TABLE_']) as $Vcol){
                        Session::clear_session($Vcol);
                    }
                    Session::clear_session('my_rand,'._CONFIG_['_ELW_']);
                    header("Location: "._CONFIG_['_LOGOUT_']."?er=1");
                    exit();
                }
            } else {
                return true;
            }
            foreach(DB::name_column(_CONFIG_['_TABLE_']) as $Vcol){
                Session::clear_session($Vcol);
            }
            Session::clear_session('my_rand,'._CONFIG_['_ELW_']);
            header("Location: "._CONFIG_['_LOGOUT_']."?er=1");
        } else {
            if(!in_array($_SESSION[DB::name_column(_CONFIG_['_TABLE_'])[4]], $sezione)) {
                foreach(DB::name_column(_CONFIG_['_TABLE_']) as $Vcol){
                    Session::clear_session($Vcol);
                }
                Session::clear_session('my_rand,'._CONFIG_['_ELW_']);
                header("Location: "._CONFIG_['_LOGOUT_']."?er=1");
            }

        }
    }

    // Redirect in base alla categoria di appartenenza
    private function Redirect($categoria) {
        header("Location: "._CONFIG_['_SITE_'].$categoria[$_SESSION[DB::name_column(_CONFIG_['_TABLE_'])[4]]]);// potrebbe inviarmi ad esempio in www.regololab.it/admin/
        exit();
    }
}
// -----------------------------------------------------------------------------------------------------
    /*

    1-> Tipologia di utenti che posso accere
    2-> Livello di protezione della pagina
    3-> Se inserito questo valore reindirizzer� una tipologia utente in una pagina specifica
    */
    # Pagina di Login: ------------
    # if($_POST['username'] && $_POST['password']){Login::protection(array('user', 'admin'), array(9, 10), array('user'=>'dashboard/', 'admin'=>'test/'));}
    # LogIn::logout();
    # Altre Pagine: ---------------
    # LogIn::protection(array('admin'), array(9,10), false);
    # LogIn::logout();
?>
