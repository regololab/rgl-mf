<?php
class PgSql {

    protected static $query;

    public function ConnectBdPgSql() {
    $db = @pg_connect('host='._CONFIG_['_DB_']['_HOST_PG_'].' port='._CONFIG_['_DB_']['_PORT_PG_'].' dbname='._CONFIG_['_DB_']['_NAME_PG_'].' user='._CONFIG_['_DB_']['_USER_PG_'].' password='._CONFIG_['_DB_']['_PASS_PG_'].'');
    if(!$db) die('DATABASE error connection');
	    return $db;
    }

    public function Query($sting, $description_err = false) { // $error = true or false
	    if(_CONFIG_['_DB_']['_ERR_QUERY']){
		    // crea una query con messaggio di errore, nel caso si verificasse
		    self::$query = @pg_query(self::ConnectBdPgSql(), $sting) or die ($description_err.': <br/><span style="color:red;">'.$sting.'</span><br/>'.self::error_db());
	    } else {
		    // crea una query senza messaggio di errore
		    self::$query = @pg_query(self::ConnectBdPgSql(), $sting);
	    }
	    return self::$query;
    }

    public function fetch($type) {
        $type = strtolower($type);
        if($type == 'row'){
            return @pg_fetch_row(self::$query);
        } elseif($type == 'assoc') {
            return @pg_fetch_assoc(self::$query);
        } elseif($type == 'obj' || $type == 'object' ) {
            return @pg_fetch_object(self::$query);
        } elseif($type == 'array') {
            return @pg_fetch_array(self::$query);
        }
    }
    
    function result($type = false) {
	$R = array();
	if($type) $type = strtolower($type);
	else $type = 'assoc';
	while($X = self::fetch($type)) {
	    $R[] = $X;
	}
	return $R;
    }
       
    public function error_db() {return @pg_last_error(self::ConnectBdPgs());}
    // istanza pg_num_rows
    public function n_rows() {return @pg_num_rows(self::$query);}
    // restituisce l'ultimo id inserito
    public function get_last_id() {return self::result('assoc');}
    // disconnette il database
    public function disconnect() {return @pg_close(self::$query);}
    // inserisce gli escape nei dati
    public function escape_string($str) {return pg_escape_string($str);}
    // svuota la tabella e riparte da 1
    public function truncate($table) {$sql="TRUNCATE TABLE ".$table.""; self::$query($sql);}
    // elimina la tabella
    public function drop($table) {$sql="DROP TABLE ".$table.""; self::$query($sql);}
    // restituisce tutti i record

    public function __destruct() {
	    @pg_free_result(self::$query);
    }

}
?>
