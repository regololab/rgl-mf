<?php

class Restful {

    function send_data($url, $data_json, $method="POST", $array=0){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Chromium");
        curl_setopt($ch, CURLOPT_URL, $url);
        #curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $response  = curl_exec($ch);
        curl_close($ch);
        if($array==1){
            $response_to_array = json_decode($response, 1);
            return $response_to_array;
        } else {
            return $response;
        }
    }

    function get_(){
        $method = htmlspecialchars($_SERVER['REQUEST_METHOD']);
        $request = @explode('/', trim(htmlspecialchars($_SERVER['PATH_INFO']),'/'));
        $input = json_decode(file_get_contents('php://input'),true);
        if($method == GET){
            return array(
                'method'=>$method,
                'request'=>$request,
                'input'=>$_GET,
            );
        }
    }

    function post_(){
        $method = htmlspecialchars($_SERVER['REQUEST_METHOD']);
        $request = @explode('/', trim(htmlspecialchars($_SERVER['PATH_INFO']),'/'));
        $input = json_decode(file_get_contents('php://input'),true);
        if($method == POST){
            return array(
                'method'=>$method,
                'request'=>$request,
                'input'=>$input,
            );
        }
    }

    function put_(){
        $method = htmlspecialchars($_SERVER['REQUEST_METHOD']);
        $request = @explode('/', trim(htmlspecialchars($_SERVER['PATH_INFO']),'/'));
        $input = json_decode(file_get_contents('php://input'),true);
        if($method == PUT){
            return array(
                'method'=>$method,
                'request'=>$request,
                'input'=>$input,
            );
        }
    }

    function delete_(){
        $method = htmlspecialchars($_SERVER['REQUEST_METHOD']);
        $request = @explode('/', trim(htmlspecialchars($_SERVER['PATH_INFO']),'/'));
        $input = json_decode(file_get_contents('php://input'),true);
        if($method == DELETE){
            return array(
                'method'=>$method,
                'request'=>$request,
                'input'=>$input,
            );
        }
    }
}

?>
