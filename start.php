<?php
require_once(dirname(__FILE__)."/Config/config.php");
require_once(_CONFIG_['_ROOT_']."Lib/rgl.php");
require_once(_CONFIG_['_ROOT_']."Lib/db.php");
require_once(_CONFIG_['_ROOT_']."Lib/string.php");
require_once(_CONFIG_['_ROOT_']."Lib/crypt.php");
require_once(_CONFIG_['_ROOT_']."_start_/start.php");

 ?>
 <!DOCTYPE html>
 <html>
     <head>
         <meta charset="utf-8" />
         <title>RGL Mini-Framework</title>
         <meta name="AUTHOR" content="" />
         <meta name="description" content="" />
         <meta name="keywords" content="" />
         <link rel="icon" href="./img/favicon.svg" type="image/svg" />
         <!--  EXTERNAL CSS (bootstrap, Foundation, Skeleton...) -->
         <link rel="stylesheet" type="text/css" href="./css/css.php" >
         <style type="text/css">
         .blink_text {animation: blinker 5s linear infinite;color:rgb(46, 122, 201);}
         @keyframes blinker {50% { opacity: 0; }}
         </style>
         <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g=" crossorigin="anonymous"></script>
     </head>
 <body style="padding:10px;">
     <h1 style="background:#002255;padding:5px;text-align:center;">
         <img src="./img/rgl_mf.svg" alt="logo RGL Framework PHP - MTV" style="width:25%;">
         <a href="https://regololab.com" style="color:#fff;text-decoration:none;">by <img src="./img/regololab.svg" alt="logo Regololab Framework PHP - MTV" style="width:5%;"></a>
     </h1><br>

     <?php if (substr(sprintf('%o', fileperms('./db/')), -4) == '0777'): ?>
         <b>Directory db: <span style="color:rgb(30, 143, 11);">OK</span> </b><br>
     <?php else: ?>
         <b>Directory db: <span style="color:rgb(198, 18, 18);">KO</span> (<?php echo substr(sprintf('%o', fileperms('./db/')), -4) ?>)</b><br>
     <?php endif; ?>

    <?php if (substr(sprintf('%o', fileperms('./Model/')), -4) == '0777'): ?>
        <b>Directory Model: <span style="color:rgb(30, 143, 11);">OK</span> </b><br>
    <?php else: ?>
        <b>Directory Model: <span style="color:rgb(198, 18, 18);">KO</span> (<?php echo substr(sprintf('%o', fileperms('./Model/')), -4) ?>)</b><br>
    <?php endif; ?>

    <?php if (substr(sprintf('%o', fileperms('./View/')), -4) == '0777'): ?>
        <b>Directory View: <span style="color:rgb(30, 143, 11);">OK</span> </b><br>
    <?php else: ?>
        <b>Directory View: <span style="color:rgb(198, 18, 18);">KO</span> (<?php echo substr(sprintf('%o', fileperms('./View/')), -4) ?>)</b><br>
    <?php endif; ?>

    <?php if (substr(sprintf('%o', fileperms('./Templates/')), -4) == '0777'): ?>
        <b>Directory Templates: <span style="color:rgb(30, 143, 11);">OK</span> </b><br>
    <?php else: ?>
        <b>Directory Templates: <span style="color:rgb(198, 18, 18);">KO</span> (<?php echo substr(sprintf('%o', fileperms('./Templates/')), -4) ?>)</b><br>
    <?php endif; ?>

    <?php if (substr(sprintf('%o', fileperms('./_start_/')), -4) == '0777'): ?>
        <b>Directory _start_: <span style="color:rgb(30, 143, 11);">OK</span> </b><br>
    <?php else: ?>
        <b>Directory _start_: <span style="color:rgb(198, 18, 18);">KO</span> (<?php echo substr(sprintf('%o', fileperms('./_start_/')), -4) ?>)</b><br>
    <?php endif; ?>

    <?php if (substr(sprintf('%o', fileperms('./img/')), -4) == '0777'): ?>
        <b>Directory img: <span style="color:rgb(30, 143, 11);">OK</span> </b><br>
    <?php else: ?>
        <b>Directory img: <span style="color:rgb(198, 18, 18);">KO</span> (<?php echo substr(sprintf('%o', fileperms('./img/')), -4) ?>)</b><br>
    <?php endif; ?>

    <?php if (substr(sprintf('%o', fileperms('./Config/generate.php')), -4) == '0777'): ?>
        <b>File ./Config/generate.php: <span style="color:rgb(30, 143, 11);">OK</span> </b><br>
    <?php else: ?>
        <b>File ./Config/generate.php: <span style="color:rgb(198, 18, 18);">KO</span> (<?php echo substr(sprintf('%o', fileperms('./Config/generate.php')), -4) ?>)</b><br>
    <?php endif; ?>

    <?php if (substr(sprintf('%o', fileperms('./Config/config.php')), -4) == '0777'): ?>
        <b>File ./Config/config.php: <span style="color:rgb(30, 143, 11);">OK</span> </b><br>
    <?php else: ?>
        <b>File ./Config/config.php: <span style="color:rgb(198, 18, 18);">KO</span> (<?php echo substr(sprintf('%o', fileperms('./Config/config.php')), -4) ?>)</b><br>
    <?php endif; ?>

    <?php if (substr(sprintf('%o', fileperms('./start.php')), -4) == '0777'): ?>
        <b>File ./start.php: <span style="color:rgb(30, 143, 11);">OK</span> </b><br>
    <?php else: ?>
        <b>File ./start.php: <span style="color:rgb(198, 18, 18);">KO</span> (<?php echo substr(sprintf('%o', fileperms('./start.php')), -4) ?>)</b><br>
    <?php endif; ?>

    <hr>
    <?php if(CreateConfig::verifica_perms()): ?>
        <form class="" action="./start.php" method="post">
            <b>Assegna prefisso alle tabelle del Database:</b><br><br>
            <input type="text" name="prefix" value="rgl_" placeholder="eg: rgl_" style="width:30%;"> <br><br>

            <td><b>Crea tabella Login:</b></td>
            <td><input type="checkbox" name="login" value="1" checked="checked" data-check="login"><br></td>
            <table id="campi_login">
                <tr>
                    <td><b>Email:</b></td>
                    <td><input type="email" name="email" value="" placeholder="eg: tuaemail@tuodominio.it" style="width:250px;"></td>
                </tr>
                <tr>
                    <td><b>Password:</b></td>
                    <td> <input type="password" name="pass1" value="" placeholder="eg: tuapassword" style="width:250px;"></td>
                </tr>
                <tr>
                    <td><b>Ripeti Password:</b> &nbsp;&nbsp;&nbsp;</td>
                    <td> <input type="password" name="pass2" value="" placeholder="eg: tuapassword" style="width:250px;"></td>
                </tr>
                <tr>
                    <td><b>Nome:</b></td>
                    <td> <input type="text" name="nome" value="" placeholder="eg: tuonome" style="width:250px;"></td>
                </tr>
                <tr>
                    <td><b>Cognome:</b></td>
                    <td> <input type="text" name="cognome" value="" placeholder="eg: tuocognome" style="width:250px;"></td>
                </tr>
            </table>
            <br><br>


            <b>Crea tabella Lingue:</b>
            <input type="checkbox" name="lingue" value="1" checked="checked"> <br><br>
            <input type="submit" name="generate_db" value="GENERA DATABASE" id="submit"> <span class="blink_text" style="display:none;">Generazione in corso... attendere.</span>
        </form>

    <?php else: ?>
        <h2 style="color:rgb(198, 18, 18);font-size:20px;">Attenzione: assegna i permessi -rwx-rwx-rwx- (777) alle directory o file con risultato KO</h2>
        <?php if(strtoupper(substr(PHP_OS, 0, 3)) != 'WIN'): ?>
            Per velocizzare la procedura puoi utilizzare il file <b>modifica_permessi.sh</b> <br><br>
        <?php endif; ?>

    <?php endif; ?>


     <script type="text/javascript" src="./js/rgl.js"></script>
     <script type="text/javascript">
        $('[data-check="login"]').on('click', function() {
            if($('[data-check="login"]').is(':checked')) {
                $('#campi_login').show(250);
            } else {
                $('#campi_login').hide(250);
                $("input[name='email']").val('');
                $("input[name='pass1']").val('');
                $("input[name='pass2']").val('');
                $("input[name='nome']").val('');
                $("input[name='cognome']").val('');
            }
        });
        $('#submit').on('click', function(){
            $('.blink_text').show(255);
        })
     </script>
 </body>
 </html>

 <!--
 RGL mini framework
 LICENSE MIT:
 Copyright <?php echo date('Y');?> Regololab.com
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 Except as contained in this notice, the name(s) of the above copyright holders shall not be used in advertising or otherwise to promote the sale,
 use or other dealings in this Software without prior written authorization.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 -->
