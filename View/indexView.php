<?php
// aggiungo nell'array i percorsi dei file ed i nomi delle classi che mi servono per gestire questa pagina
$GLOBALS["Rgl"] = new $nameClass(
    array(
        "Crypto", // gestione Criptazione
        "Session", // gestione delle sessioni
        "Language", // gestione delle lingue
        "Avvisi", // gestore avvisi e alert
        "Str", // trattamento delle stringhe
        "DTime", // trattamento delle date
        "VerifySend", // verifica i dati in invio
        #"SendMail", // invia email in vari formati
        "DB", // gestione del DB in PDO comprende (MySql, SqLite, PostgreSql)
        #"MySql", // gestione del DB Mysql
        #"SqLite", // gestione del DB SqLite
        #"PgSql", // gestione del DB PstgreSql
        "BreadC", // gestione briciole di pane
        #"UploadFile", // Upload File & image
        "LogIn", // gestione Login
        #"Files", // gestione dei file fisici
        #"Restful" // ,
    )
);

class IndexView extends IndexModel {

    # gestione contenuto pagina ####
    function view_page(){
        ### stampo la pagina dei contenuti
        KuiperBelt::get_template(
            'index.php',
            $GLOBALS["Rgl"],
            array(
                'benvenuto'=>Language::translate()['welcome_rgl'],
                'generate'=>Language::translate()['generate_page'],
                'logins'=>$logins,
            )
        );
    }
}

IndexView::view_page();
?>
