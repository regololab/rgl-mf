<?php
include(_CONFIG_['_ROOT_'].'Lib/postgresql.php');
class CreateConfig extends DB{

    function verifica_perms(){
        if (
            substr(sprintf('%o', fileperms('./Model/')), -4) == '0777' &&
            substr(sprintf('%o', fileperms('./db/')), -4) == '0777' &&
            substr(sprintf('%o', fileperms('./View/')), -4) == '0777' &&
            substr(sprintf('%o', fileperms('./Templates/')), -4) == '0777' &&
            substr(sprintf('%o', fileperms('./_start_/')), -4) == '0777' &&
            substr(sprintf('%o', fileperms('./img/')), -4) == '0777' &&
            // if you work in windows with xampp, comment these three rows
            substr(sprintf('%o', fileperms('./Config/generate.php')), -4) == '0777' &&
            substr(sprintf('%o', fileperms('./Config/config.php')), -4) == '0777' &&
            substr(sprintf('%o', fileperms('./start.php')), -4) == '0777'
            // and activate these two rows
            #substr(sprintf('%o', fileperms('./Config/')), -4) == '0777' &&
            #substr(sprintf('%o', fileperms('./')), -4) == '0777'
        ){
            return 1;
        } else {
            return 0;
        }
    }

    function create_db($_){
        if($_['generate_db']){
            if($_['prefix']){
                $prefisso = str_replace(" ", "", trim($_['prefix']));
            }

            $lon = 0;
            if($_['login']){
                $name_table = $prefisso.'logins';
                self::genera_tabella_login($name_table);

                $email = Str::str_clear($_['email'],1);
                $pass1 = Str::str_clear($_['pass1'],1);
                $pass2 = Str::str_clear($_['pass2'],1);
                $nome = Str::str_clear($_['nome'],1);
                $cognome = Str::str_clear($_['cognome'],1);

                if($email && $pass1 && $pass2 && $nome &&  $cognome){
                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        if($pass1 == $pass2){
                            $pass = Crypto::gen_pass($pass1);
                            $sql = "INSERT INTO {$name_table}(email, password, active, cat, level, name, surname) VALUES(
                                '".$email."',
                                '".$pass."',
                                '1',
                                'admin',
                                '10',
                                '".$nome."',
                                '".$cognome."'
                            )";
                            DB::query($sql);
                            $lon = 1;
                        } else {
                            echo "<script type=\"text/javascript\"> alert('ALERT: Le password inserite non coincidono!');</script>";
                        }
                    } else {
                        echo "<script type=\"text/javascript\"> alert('ALERT: Indirizzo Email non valido!');</script>";
                    }
                } else {
                    echo "<script type=\"text/javascript\"> alert('ALERT: Tutti i campi sono obbligatori!');</script>";
                }
            }

            $lin = 0;
            if($_['lingue']){
                $name_table_lang = $prefisso.'language';
                self::genera_tabella_lang($name_table_lang);
                $json = file_get_contents("./_start_/language.json");
                // unzip flags
                self::unzip();

                foreach(json_decode($json, 1) as $V){
                    $sql = "INSERT INTO ".$name_table_lang."(code, active, flag) VALUES(
                            '".$V['iso_2']."',
                            '0',
                            '".$V['iso_2'].".png'
                        ) ";
                    DB::query($sql);
                }
                $lin = 1;
            }

            // verifico che tutto sia andato per il meglio
            if($lon){
                $sql ="SELECT * FROM {$name_table} ";
                DB::query($sql);
                $R = DB::result();
                $n_login = count($R);
            }

            if($lin){
                $sql ="SELECT * FROM {$name_table_lang} ";
                DB::query($sql);
                $R2 = DB::result();
                $n_lang = count($R2);
            }

            if($_['login'] && $_['lingue']){
                if($lon && $lin){
                    if($n_login > 0 && $n_lang > 0){
                        self::gestione_config($prefisso);
                        #echo _CONFIG_['_ROOT_'].'start.php';
                        @unlink(_CONFIG_['_ROOT_'].'start.php');
                        @unlink(_CONFIG_['_ROOT_'].'modifica_permessi.sh');
                        self::generate_sh();
                        self::delete_files(_CONFIG_['_ROOT_'].'_start_/');
                        @rmdir(_CONFIG_['_ROOT_'].'_start_/');
                        @chmod(_CONFIG_['_ROOT_']."config/config.php", 0755);
                        @chmod(_CONFIG_['_ROOT_']."db", 0755);
                    }
                }
            }

            if($_['login'] && !$_['lingue']){
                if($lon){
                    if($n_login > 0 && $n_lang == 0){
                        self::gestione_config($prefisso);
                        @unlink(_CONFIG_['_ROOT_'].'start.php');
                        @unlink(_CONFIG_['_ROOT_'].'modifica_permessi.sh');
                        self::generate_sh();
                        self::delete_files(_CONFIG_['_ROOT_'].'_start_/');
                        @rmdir(_CONFIG_['_ROOT_'].'_start_/');
                        @chmod(_CONFIG_['_ROOT_']."config/config.php", 0755);
                        @chmod(_CONFIG_['_ROOT_']."db", 0755);

                    }
                }
            }

            if(!$_['login'] && $_['lingue']){
                if($lin){
                    if($n_login == 0 && $n_lang > 0){
                        self::gestione_config($prefisso);
                        @unlink(_CONFIG_['_ROOT_'].'start.php');
                        @unlink(_CONFIG_['_ROOT_'].'modifica_permessi.sh');
                        self::generate_sh();
                        self::delete_files(_CONFIG_['_ROOT_'].'_start_/');
                        @rmdir(_CONFIG_['_ROOT_'].'_start_/');
                        @chmod(_CONFIG_['_ROOT_']."config/config.php", 0755);
                        @chmod(_CONFIG_['_ROOT_']."db", 0755);
                    }
                }
            }
        }
    }

    function gestione_config($prefix){
        $conf = file_get_contents(_CONFIG_['_ROOT_']."Config/config.php");
        $config = str_replace(
            array(
                "{{LOGINS}}",
                "{{PREFIX}}",
                "{{ELW}}",
                "{{KYE}}",
                "{{KEY}}",
                "{{IV}}",
            ),
            array(
                $prefix."logins",
                $prefix,
                self::random_frase_kye(6),
                self::random_frase_kye(6),
                self::random_frase(32),
                self::random_frase(32),
            ), $conf);
        file_put_contents(_CONFIG_['_ROOT_']."Config/config.php", $config);
    }

    function generate_sh(){
        $sh = "#!/bin/sh\nBASEDIR=$(dirname $0)\nsudo chmod -R 777 ./Config/generate.php ./Model/ ./View/ ./Templates/ ./img/\nsudo chmod -R 755  ./db/ ./img/ ./Config/config.php";
        $file = fopen(_CONFIG_['_ROOT_']."modifica_permessi.sh", "w") or die("Unable to open file!");
        fwrite($file, $sh);
        fclose($file);
        @chmod(_CONFIG_['_ROOT_']."modifica_permessi.sh", 0777);
    }

    function genera_tabella_login($name){
        DB::drop($name);
        if(_CONFIG_['_DB_']['_TIPO_DB_']=="SQLITE"){
            $sql = " CREATE TABLE {$name} (id INTEGER PRIMARY KEY, email TEXT, password TEXT, active NUMERIC, cat TEXT, level NUMERIC, name TEXT, surname TEXT)";
            DB::query($sql);
        }
        if(_CONFIG_['_DB_']['_TIPO_DB_']=="MYSQL"){
            $sql = " CREATE TABLE `{$name}` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `email` varchar(255) CHARACTER SET utf8 NOT NULL,
                `password` varchar(50) CHARACTER SET utf8 NOT NULL,
                `active` smallint(6) NOT NULL,
                `cat` enum('admin','user','admin-user') CHARACTER SET utf8 NOT NULL,
                `level` smallint(6) NOT NULL,
                `name` varchar(255) CHARACTER SET utf8 NOT NULL,
                `surname` varchar(255) CHARACTER SET utf8 NOT NULL,
                PRIMARY KEY (id))
                ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            DB::query($sql);

        }
        if(_CONFIG_['_DB_']['_TIPO_DB_']=="PGSQL"){
            $sql = "
                DROP SEQUENCE IF EXISTS serial CASCADE;
                CREATE SEQUENCE serial;
                CREATE TABLE {$name}
                (
                  id integer PRIMARY KEY DEFAULT nextval('serial'),
                  email character varying(255) NOT NULL,
                  password character varying(50) NOT NULL,
                  active integer NOT NULL,
                  cat character varying(50) NOT NULL,
                  level integer NOT NULL,
                  name character varying(255) NOT NULL,
                  surname character varying(255) NOT NULL,
                  CONSTRAINT auth_user_username_key UNIQUE (email)
                );
            ";
            PgSql::query($sql);
        }
    }

    function genera_tabella_lang($name){
        DB::drop($name);
        if(_CONFIG_['_DB_']['_TIPO_DB_']=="SQLITE"){
            $sql = " CREATE TABLE {$name} (id INTEGER PRIMARY KEY, code TEXT, active NUMERIC, flag TEXT)";
            DB::query($sql);
        }
        if(_CONFIG_['_DB_']['_TIPO_DB_']=="MYSQL"){
            $sql = " CREATE TABLE `{$name}` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `code` varchar(2) CHARACTER SET utf8 NOT NULL,
                `active` smallint(1) NOT NULL,
                `flag` varchar(255) CHARACTER SET utf8 NOT NULL,
                PRIMARY KEY (id))
                ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            DB::query($sql);

        }
        if(_CONFIG_['_DB_']['_TIPO_DB_']=="PGSQL"){
            $sql = "
                DROP SEQUENCE IF EXISTS serial_lang CASCADE;
                CREATE SEQUENCE serial_lang;
                CREATE TABLE {$name}
                (
                  id integer PRIMARY KEY DEFAULT nextval('serial_lang'),
                  code character varying(2) NOT NULL,
                  active integer NOT NULL,
                  flag character varying(255) NOT NULL
                )
            ";
            PgSql::query($sql);
        }
    }

    function unzip(){
        $zip = new ZipArchive;
        if ($zip->open(_CONFIG_['_ROOT_'].'_start_/flags.zip') === TRUE) {
            $zip->extractTo(_CONFIG_['_ROOT_'].'_start_/flags/');
            $zip->close();
        }
        rename(_CONFIG_['_ROOT_']."_start_/flags/", _CONFIG_['_ROOT_']."img/flags/");
        @chmod(_CONFIG_['_ROOT_']."img/flags", 0777);
    }

    function delete_files($dir) {
        if(is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir"){
                        rmdir($dir."/".$object);
                    }else{
                        unlink($dir."/".$object);
                    }
                }
             }
            reset($objects);
            rmdir($dir);
        }
    }

    function random_frase($number){
        $frase = str_shuffle("._abcdefghijklmnopqrstuvwxyz*+-:;,!$(?)*ABCDEFGHIJKLMNOPQRSTUVWXYZ*+-:;,!$(?)*0123456789*+-:;,!$(?)*");
        $estrai = "";
        for($i=0;$i < $number; $i++){
            $estrai .= $frase[$i];
        }
        return $estrai;
    }

    function random_frase_kye($number){
        $frase = str_shuffle("_abcdefghijkl_mnopqrstuvwxy_ABCDEFGHIJKLMN_OPQRSTUVWXYZ_0123456789");
        $estrai = "";
        for($i=0;$i < $number; $i++){
            $estrai .= $frase[$i];
        }
        return $estrai;
    }
}
$CreateConfig = new CreateConfig();
$CreateConfig->create_db($_POST);

if(!file_exists(_CONFIG_['_ROOT_'].'_start_/')){
    header('Location: '._CONFIG_['_SITE_']);
}
 ?>
