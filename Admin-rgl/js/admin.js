$(document).ready(function() {
    $("#page").keypress(function(evt) {
        var charCode = (evt.which)?evt.which:event.keyCode;
        if (charCode == 95 || charCode == 8 || charCode == 45 || (charCode > 96 && charCode < 123) || (charCode > 47 && charCode < 59)) {
            return true;
        } else {
            return false;
        }
    });

    $('#page').keyup(function(){
        if($('#page').val() == ''){
            $('#view').val('');
            $('#model').val('');
            $('#pagina').val('');
            return false;
        }
        var page = $('#page').val();
        var pages = $('#page').val();
        pages = pages.split('_').join('@');
        pages = pages.split('-').join('@');
        pages = pages.split("@");

        var pagex = '';
        for(i = 0; i < pages.length; i++){
            pagex += capitaliseFirstLetter(pages[i]);
        }
        $('#view').val(pagex+'View');
        $('#model').val(pagex+'Model');
        $('#pagina').val(lowerFirstLetter(page)+'.php');
    });
});

function capitaliseFirstLetter(string){
    return string[0].toUpperCase() + string.slice(1);
}
function lowerFirstLetter(string){
    return string[0].toLowerCase() + string.slice(1);
}
