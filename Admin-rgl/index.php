<?php
include("../Config/config.php");
include("./include/login.php");
include("./include/generate.php");
$generate = new Generate($kuiper);
$generate->DeleteClass();
$generate = $generate->createClass() ;
#echo "<br><hr>";
include(_CONFIG_['_CONF_']."generate.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Genera Classi</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" >
        <link rel="stylesheet" type="text/css" href="./css/base.css" >
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <!-- Internet Explorer 10 in Windows Phone 8 -->
        <script type="text/javascript">
            // Copyright 2014-2017 The Bootstrap Authors
            // Copyright 2014-2017 Twitter, Inc.
            // Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
            if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
              var msViewportStyle = document.createElement('style')
              msViewportStyle.appendChild(
                document.createTextNode(
                  '@-ms-viewport{width:auto!important}'
                )
              )
              document.head.appendChild(msViewportStyle)
            }
        </script>
    </head>

<body>
    <!-- Fixed navbar -->
   <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
     <h3 class="text-white"><i class="fa fa-file-code-o" aria-hidden="true"></i> Generazione Pagine</h3>
   </nav>

   <div class="bd-pageheader">
        <div class="container">
        <h1>&nbsp;</h1><p class="lead">&nbsp;</p>
        </div>
    </div>

   <div class="container-fluid">
    <?php if ($generate == -1): ?>
        <div class="alert alert-danger alert-dismissable">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Attenzione:</strong> Questa voce &egrave; gi&agrave; presente!
        </div>
    <?php endif; ?>
    <?php if ($generate == 1): ?>
        <div class="alert alert-success alert-dismissable">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>OK:</strong> Generazione avvenuta correttamente.
        </div>
    <?php endif; ?>

    <form class="container-fluid" action="index.php" method="post">
    <div class="row">
            <div class="col-8">
                <input type="text" class="form-control" id="page" name="page" aria-describedby="pageHelp" placeholder="eg.: news-events" autocomplete="off">
            </div>
            <div class="col-4">
                <button type="submit" id="submits" class="btn btn-primary" ><i class="fa fa-save"></i> Genera </button>
                <button type="reset" data-reset="true" class="btn btn-warning" ><i class="fa fa-refresh"></i> Svuota </button>
            </div>
    </div>

    <div class="row">&nbsp;</div>
    <div class="col-12" >
        <table class="table table-striped table-condensed">
          <thead class="thead-inverse">
            <tr class="something">
              <th>Class Model</th>
              <th>Page Templates</th>
              <th>Class View</th>
            </tr>
          </thead>
          <tbody>
            <tr class="something">
              <th scope="row"><input type="text" name="model" value="" id="model" readonly="readonly" style="background:transparent;border:none;font-weight:bold;color:#2D4ACE;"  /></th>
              <td><input type="text" name="pagina" value="" id="pagina" readonly="readonly" style="background:transparent;border:none;font-weight:bold;color:#2D4ACE;"  /></td>
              <td><input type="text" name="view" value="" id="view" readonly="readonly" style="background:transparent;border:none;font-weight:bold;color:#2D4ACE;" /></td>
            </tr>
          </tbody>
        </table>
    </div>
    </form>

    <div class="row">&nbsp;</div>

    <div class="col-12">
        <h3 class="text-primary"><i class="fa fa-folder-o"></i> Pagine/Classi Generate</h3>
        <table id="example2" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
          <thead class="thead-inverse2">
            <tr class="something">
              <th>Percorso</th>
              <th>Model</th>
              <th>Templates</th>
              <th>View</th>
              <th>Azioni</th>
            </tr>
          </thead>
          <tbody>
            <?php sort($kuiper);foreach($kuiper as $V):?>
            <tr class="something">
              <td><?php echo ($V[0] !='index') ? ($V[0].'/') : ('/');?></td>
              <td><?php echo str_replace('.php', '', ucfirst(ucfirst($V[3])));?></td>
              <td><?php echo $V[1];?></td>
              <td><?php echo str_replace('.php', '', ucfirst(ucfirst($V[2])));?></td>
              <td>
                    <?php if($V[0] != 'index'): ?>
                        <a href="#0" class="btn btn-danger btn-sm" onclick="return Confirm('<?php echo $V[0];?>');" title="cancella!">
                            <i class="fa fa-trash"></i>
                        </a>
                    <?php endif; ?>
              </td>
            </tr>
            <?php endforeach;?>
          </tbody>
        </table>
    </div>

    </div>

<script type="text/javascript" src="js/admin.js"></script>
<script type="text/javascript">
function Confirm(f) {
    var r = confirm("Sicuro di voler cancellare il valore selezionato?");
    if (r==true) {
        location.href='<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>?percorso='+f+'&canc=1;'
    } else {
        return false;
    }
}
$('[#data-reset="true"]').on('click, touchstart, touch', function(){
    $('#model').val('');
    $('#pagina').val('');
    $('#view').val('');
})
</script>

</n>
</html>
