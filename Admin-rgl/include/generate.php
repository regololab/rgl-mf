<?php
class Generate {

    public function __construct($arr) {
        $this->arr = $arr;
    }

    public function createClass() {
        if($_POST['page'] && $_POST['model'] && $_POST['view'] && $_POST['pagina']) {
            $page = file_get_contents(_CONFIG_['_CONF_'].'generate.php');
            $in_generate = self::ClassPage();
            $createArray = "    array('".$_POST['page']."', '".$_POST['pagina']."', '".Generate::lowCFirst($_POST['model']).".php', '".Generate::lowCFirst($_POST['view']).".php'),\n##:NewObject:##";
            $text = str_replace('##:NewObject:##', $createArray, $page);
            $verifica_nome = Generate::VerificaClasse($_POST, $in_generate);
            if($verifica_nome) {return -1;}

            Generate::createFile(_CONFIG_['_CONF_'].'generate.php', $text);
            Generate::generazioneFile($_POST);
            self::assign_chmod(_CONFIG_['_CONF_'].'generate.php', $valore=0777);

            return 1;
        }
    }

    public function deleteClass() {
        if($_GET['percorso'] && $_GET['canc']) {
            $percorso = $_GET['percorso'];
            $pagina = $percorso.'.php';

            $className = str_replace(array('-','_'), '@', $percorso);
            $classN = explode('@', $className);
            $clax = '';
            foreach($classN as $V) {
                $clax .= ucfirst($V);
            }

            $model_file = Generate::lowCFirst($clax).'Model.php';
            $view_file = Generate::lowCFirst($clax).'View.php';

            @unlink(_CONFIG_['_ROOT_'].'Templates/'.$pagina);
            @unlink(_CONFIG_['_ROOT_'].'Model/'.$model_file);
            @unlink(_CONFIG_['_ROOT_'].'View/'.$view_file);
            #@rmdir(_CONFIG_['_ROOT_'].'Templates/'.$percorso);

            $text = Generate::deleteRows($percorso, $pagina, $model_file, $view_file);
            Generate::createFile(_CONFIG_['_CONF_'].'generate.php', $text);
            self::assign_chmod(_CONFIG_['_CONF_'].'generate.php', $valore=0777);
        }
    }

    private function deleteRows($percorso, $pagina, $model, $view) {
        $file = file(_CONFIG_['_CONF_'].'generate.php');
        $filex = '';
        foreach($file as $V){
            if (strpos($V, "'".$percorso."',") === false) {
                $filex .= $V;
            }
        }
        return $filex;
    }

    private function generazioneFile($arr) {
        $page = $arr['page'];
        $model = $arr['model'];
        $view = $arr['view'];
        $pagina = $arr['pagina'];

        $textModel='<?php
class '.$model.' extends Rgl {
    # No __construct

}
?>';
        $fileModel = Generate::lowCFirst($model);
        Generate::createFile(_CONFIG_['_ROOT_'].'Model/'.$fileModel.'.php', $textModel);
        self::assign_chmod(_CONFIG_['_ROOT_'].'Model/'.$fileModel.'.php', $valore=0777);
        ############################################################

        $textView='<?php
// Codifica caratteri
header("Content-Type: text/html; charset=utf-8");
#header("Content-type: application/json");
$GLOBALS["Rgl"] = new $nameClass(
    array(
        "Crypto", // gestione Criptazione
        "Session", // gestione delle sessioni
        "Language", // gestione delle lingue
        "Avvisi", // gestore avvisi e alert
        "Str", // trattamento delle stringhe
        "DTime", // trattamento delle date
        "VerifySend", // verifica i dati in invio
        #"SendMail", // invia email in vari formati
        "DB", // gestione del DB in PDO comprende (MySql, SqLite, PostgreSql)
        #"MySql", // gestione del DB Mysql
        #"SqLite", // gestione del DB SqLite
        #"PgSql", // gestione del DB PstgreSql
        "BreadC", // gestione briciole di pane
        #"UploadFile", // Upload File & image
        "LogIn", // gestione Login
        #"Files", // gestione dei file fisici
        #"Restful" // request and response restful,
    )
);

class '.$view.' extends '.$model.' {

    # gestione contenuto pagina ####
    function view_page(){
        ### stampo la pagina dei contenuti
        KuiperBelt::get_template(
            \''.$pagina.'\',
            $GLOBALS["Rgl"],
            array(
                \'message\'=>\'\',
                \'benvenuto\'=>\'Regolo mini-framework\',
            )
        );
    }
}
'.$view.'::view_page();
?>';
        $fileView = Generate::lowCFirst($view);
        Generate::createFile(_CONFIG_['_ROOT_'].'View/'.$fileView.'.php', $textView);
        self::assign_chmod(_CONFIG_['_ROOT_'].'View/'.$fileView.'.php', $valore=0777);
        ##############################################################
        $textView = '&#9733; '._CONFIG_['_PRODUCT_'].' - '._CONFIG_['_VERSION_'].' => Page: '.$pagina."\n\n";
        Generate::createFile(_CONFIG_['_ROOT_'].'Templates/'.$pagina, $textView);
        #mkdir(_CONFIG_['_ROOT_'].'Templates/'.$page, 0777);
        self::assign_chmod(_CONFIG_['_ROOT_'].'Templates/'.$pagina, $valore=0777);
        #self::assign_chmod(_CONFIG_['_ROOT_'].'Templates/'.$page, $valore=0777);
    }

    public function lowCFirst($string) {
        return  strtolower($string{0}).substr($string, 1);
    }

    private function ClassPage() {
        $file = file(_CONFIG_['_CONF_'].'generate.php');
        return $file;
    }

    private function VerificaClasse($arr, $page) {
        foreach($page as $V) {
            if (strpos($V, "'".$arr['page']."',") !== false) {
                return 1;
            }
        }
    }

    private function createFile($page, $text) {
        $f = fopen($page ,"w");
        fwrite($f,$text);
        fclose($f);
    }

    private function assign_chmod($file, $valore=0755){
        chmod($file, $valore);
    }
}

?>
