<?php
if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header("WWW-Authenticate: Basic realm=\"Private Area\"");
    header("HTTP/1.0 401 Unauthorized");
    die("Non hai il permesso per accedere in quest'area!\n");
} else {
    if ((htmlspecialchars($_SERVER['PHP_AUTH_USER']) != _CONFIG_['_USERN_']) && (htmlspecialchars($_SERVER['PHP_AUTH_PW']) != _CONFIG_['_PASSW_'])) {
        header("WWW-Authenticate: Basic realm=\"Private Area\"");
        header("HTTP/1.0 401 Unauthorized");
        die("Non hai il permesso per accedere in quest'area!\n");
    }
}
 ?>
