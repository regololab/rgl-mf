<?php
if(file_exists('./start.php')){header("Location: start.php");}
require_once(dirname(__FILE__)."/Config/config.php");
require_once(_CONFIG_['_ROOT_']."Lib/rgl.php");
require_once(_CONFIG_['_ROOT_']."Lib/kuiper_belt.php");
require_once(_CONFIG_['_CONF_']."generate.php");
$kuiperBelt = new KuiperBelt($kuiper);
$kuiperBelt->kuiperBelt();
?>
<!--
Regolo mini framework
LICENSE MIT:
Copyright <?php echo date('Y');?> Regololab.com
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

Except as contained in this notice, the name(s) of the above copyright holders shall not be used in advertising or otherwise to promote the sale,
use or other dealings in this Software without prior written authorization.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-->
